//
//  CGCanvas.swift
//  KLine
//
//  Created by Tony Wu on 2023/9/23.
//

import Foundation
import UIKit

class CGCanvas: UIView, Canvas {
    let colors: [UIColor] = [.red, .orange, .yellow, .green, .blue, .cyan, .purple, .black]
    private var context: CGContext?
    private var rightBeforeScaling: CGFloat = 0
    private let minScale: CGFloat = 1
    private let maxScale: CGFloat = 8
    private let collectionView: UICollectionView
    private let collectionViewLayout = UICollectionViewFlowLayout()

    var pinchGestureRecognizer: UIPinchGestureRecognizer?

    var panGestureRecognizer: UIPanGestureRecognizer {
        collectionView.panGestureRecognizer
    }
    var scale: CGFloat = 1
    var contentOffset: CGPoint = .zero {
        didSet {
            collectionView.contentOffset = contentOffset
        }
    }
    var size: CGSize {
        bounds.size
    }

    weak var delegate: CanvasDelegate?
    private var targetY: CGFloat = 0
    private var animatingScale: CGFloat = 1

    required init?(coder: NSCoder) {
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.estimatedItemSize = .zero
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.backgroundColor = .clear
        collectionView.bounces = false
        collectionView.showsHorizontalScrollIndicator = false
        super.init(coder: coder)
        collectionView.register(StubCell.self, forCellWithReuseIdentifier: "StubCell")
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(collectionView)
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        collectionView.dataSource = self
        collectionView.delegate = self
        layer.drawsAsynchronously = true

        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pinchAction))
        pinch.delegate = self
        addGestureRecognizer(pinch)
        pinchGestureRecognizer = pinch
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        self.context = context
        context.translateBy(x: -collectionView.contentOffset.x, y: rect.height)
        context.scaleBy(x: 1, y: -1)

        let visibleRange = CGRect(x: collectionView.contentOffset.x, y: 0, width: rect.width , height: rect.height)
        delegate?.draw(canvas: self, in: visibleRange, animatingScale: animatingScale)
        if animatingScale != 1 {
            animatingScale = animatingValue(current: animatingScale, target: 1)
            DispatchQueue.main.async {
                self.setNeedsDisplay()
            }
        }
    }

    func refresh() {
        collectionView.reloadData()
        setNeedsDisplay()
    }

    func refreshAnimated() {
        animatingScale = 0
        collectionView.reloadData()
        setNeedsDisplay()
    }

    func save() {
        context?.saveGState()
    }

    func restore() {
        context?.restoreGState()
    }

    func clip(to rect: CGRect) {
        context?.clip(to: rect)
    }

    private func animatingValue(current: CGFloat, target: CGFloat, factor: CGFloat = 0.15) -> CGFloat {
        let diff = abs(current - target)
        if (diff < 0.005) {
            if (diff < 0.001) {
                return target
            } else {
                let result = current + (target - current).significand * 0.0003
                if (result <= 0) {
                    return target
                } else {
                    return result
                }
            }
        } else {
            let result = current + (target - current) * factor
            if (result <= 0) {
                return target
            } else {
                return result
            }
        }
    }

    func drawText(_ text: String, center: CGPoint, textSize: CGFloat, weight: UIFont.Weight,  color: UIColor) {
        guard let context = context else {
            return
        }
        context.saveGState()
        let attributedString = NSAttributedString(string: text, attributes: [.font: UIFont.systemFont(ofSize: textSize, weight: weight), .foregroundColor: color])
        let line = CTLineCreateWithAttributedString(attributedString)
        let bounds = CTLineGetImageBounds(line, context)
        let stringRect = CGRect(x: center.x - bounds.width / 2, y: center.y - bounds.height / 2, width: bounds.width, height: bounds.height)
        context.textPosition = stringRect.origin
        CTLineDraw(line, context)
    }


    func drawLine(_ startPoint: CGPoint, _ endPoint: CGPoint, lineWidth: CGFloat, color: UIColor) {
        guard let context = context else {
            return
        }

        context.setLineWidth(lineWidth)
        context.setStrokeColor(color.cgColor)
        context.beginPath()
        context.addLines(between: [startPoint, endPoint])
        context.strokePath()
    }

    func drawRect(_ rect: CGRect, color: UIColor) {
        guard let context = context else {
            return
        }

        context.setFillColor(color.cgColor)
        context.fill(rect)
    }

    func drawDashLine(_ startPoint: CGPoint, _ endPoint: CGPoint, lineWidth: CGFloat, color: UIColor) {
        guard let context = context else {
            return
        }

        context.saveGState()
        context.setLineWidth(lineWidth)
        context.setLineDash(phase: 2, lengths: [2, 1])
        context.setStrokeColor(color.cgColor)
        context.beginPath()
        context.addLines(between: [startPoint, endPoint])
        context.strokePath()
        context.restoreGState()
    }

    func drawPath(path: UIBezierPath, lineWidth: CGFloat, color: UIColor) {
        guard let context = context else {
            return
        }
        context.saveGState()
        if animatingScale != 1 {
            context.clip(to: CGRect(x: collectionView.contentOffset.x, y: 0, width: frame.width * (animatingScale), height: frame.height))
        }
        context.setLineWidth(lineWidth)
        context.setStrokeColor(color.cgColor)
        context.beginPath()
        context.addPath(path.cgPath)
        context.strokePath()

        context.restoreGState()
    }

    @objc
    private func pinchAction(_ gesture: UIPinchGestureRecognizer) {
        collectionView.panGestureRecognizer.state = .failed

        let x = collectionView.contentOffset.x + gesture.location(in: self).x
        if scale * gesture.scale < minScale {
            collectionView.contentOffset.x += x * ((minScale / scale) - 1)
            scale = minScale
        } else if scale * gesture.scale > maxScale {
            collectionView.contentOffset.x += x * ((maxScale / scale) - 1)
            scale = maxScale
        } else {
            collectionView.contentOffset.x += x * (gesture.scale - 1)
            scale *= gesture.scale
        }

        delegate?.onScale(canvas: self, scale: scale)
        gesture.scale = 1
        collectionView.reloadData()
    }
}

extension CGCanvas: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        1
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.dequeueReusableCell(withReuseIdentifier: "StubCell", for: indexPath)
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        delegate?.contentSize(canvas: self) ?? .zero
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }


    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        contentOffset = scrollView.contentOffset
        delegate?.onScroll(canvas: self, contentOffset: scrollView.contentOffset)
        setNeedsDisplay()
    }

    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        delegate?.scrollViewWillBeginDragging(canvas: self)
    }
}

extension CGCanvas: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
}
class StubCell: UICollectionViewCell {
    override var reuseIdentifier: String? {
        "StubCell"
    }
}
