//
//  Canvas.swift
//  KLine
//
//  Created by Tony Wu on 2023/9/23.
//

import Foundation
import UIKit
protocol CanvasDelegate: AnyObject {
    func scrollViewWillBeginDragging(canvas: Canvas)
    func onScroll(canvas: Canvas, contentOffset: CGPoint)
    func onScale(canvas: Canvas, scale: CGFloat)
    func draw(canvas: Canvas, in rect: CGRect, animatingScale: CGFloat)
    func contentSize(canvas: Canvas) -> CGSize
}
protocol Canvas: AnyObject {
    var scale: CGFloat { get set }
    var contentOffset: CGPoint { get set }
    var size: CGSize { get }
    func refresh()
    func drawText(_ text: String, center: CGPoint, textSize: CGFloat, weight: UIFont.Weight,  color: UIColor)
    func drawLine(_ startPoint: CGPoint, _ endPoint: CGPoint, lineWidth: CGFloat, color: UIColor)
    func drawDashLine(_ startPoint: CGPoint, _ endPoint: CGPoint, lineWidth: CGFloat, color: UIColor)
    func drawPath(path: UIBezierPath, lineWidth: CGFloat, color: UIColor)
    func drawRect(_ rect: CGRect, color: UIColor)
    func save()
    func restore()
    func clip(to rect: CGRect)
}