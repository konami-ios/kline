//
//  ViewController.swift
//  KLine
//
//  Created by Tony Wu on 2023/9/23.
//

import UIKit

public class KData {
    public let openPrice: CGFloat
    public let lowPrice: CGFloat
    public let highPrice: CGFloat
    public let closePrice: CGFloat
    public let timestamp: TimeInterval

    public init(openPrice: CGFloat, lowPrice: CGFloat, highPrice: CGFloat, closePrice: CGFloat, timestamp: TimeInterval) {
        self.openPrice = openPrice
        self.lowPrice = lowPrice
        self.highPrice = highPrice
        self.closePrice = closePrice
        self.timestamp = timestamp
    }
}

public class KLineViewController: UIViewController {
    @IBOutlet weak var symbolTitleLabel: UILabel!
    @IBOutlet var bidLabel: UILabel!
    @IBOutlet var priceChangeLabel: UILabel!
    @IBOutlet var priceChangePercentLabel: UILabel!

    @IBOutlet var openLabel: UILabel!
    @IBOutlet var highLabel: UILabel!
    @IBOutlet var closeLabel: UILabel!
    @IBOutlet var lowLabel: UILabel!

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var focusDataView: UIView!
    @IBOutlet var focusDataOpenLabel: UILabel!
    @IBOutlet var focusDataHighLabel: UILabel!
    @IBOutlet var focusDataCloseLabel: UILabel!
    @IBOutlet var focusDataLowLabel: UILabel!
    @IBOutlet var topTypeLabelView: UIView!
    @IBOutlet var topIndicatorLabel: UILabel!
    @IBOutlet var topIndicatorValueLabel: UILabel!
    @IBOutlet var topIndicatorPeriodLabel: UILabel!
    @IBOutlet var bottomIndicatorLabel: UILabel!
    @IBOutlet var bottomIndicatorValueLabel: UILabel!
    @IBOutlet var bottomIndicatorPeriodLabel: UILabel!
    @IBOutlet var bottomTypeLabelView: UIView!
    @IBOutlet var topCanvas: CGCanvas!
    @IBOutlet var bottomCanvas: CGCanvas!
    @IBOutlet var loadingView: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    private let rightPadding: CGFloat = 40
    private let initialScale: CGFloat = 8
    private let barWidth: CGFloat = 2
    private let spacing: CGFloat = 0.5
    private var contentInset: UIEdgeInsets {
        UIEdgeInsets(top: 0, left: barWidth / 2 * topCanvas.scale + 10, bottom: 12, right: rightPadding)
    }
    private var kData = [KData]()
    private let intervalItems: [KLineUpdateCommand] = [UpdateCommand(title: "分时", id: "ts", timeInterval: 60), UpdateCommand(title: "日K线", id: "1d", timeInterval: 3600 * 24),
                                                       UpdateCommand(title: "1分", id: "1m", timeInterval: 60), UpdateCommand(title: "5分", id: "5m", timeInterval: 300),
                                                       UpdateCommand(title: "15分", id: "15m", timeInterval: 900), UpdateCommand(title: "30分", id: "30m", timeInterval: 1800),
                                                       UpdateCommand(title: "1小时", id: "1h", timeInterval: 3600), UpdateCommand(title: "4小时", id: "4h", timeInterval: 3600 * 4),
                                                       UpdateCommand(title: "周线", id: "1w", timeInterval: 3600 * 24 * 7), UpdateCommand(title: "月线", id: "1M", timeInterval: 3600 * 24 * 30)]

    private let topItems: [IndicatorItem] = [MAIndicatorItem(), BOLLIndicatorItem(), BBIIndicatorItem()]
    private let bottomItems: [IndicatorItem] = [MACDIndicatorItem(), RSIIndicatorItem(), KDJIndicatorItem(), WRIndicatorItem(), ATRIndicatorItem(), CCIIndicatorItem(), KDIndicatorItem()]

    private var selectedIntervalIndex: Int = 2
    private var topSelectedIndicatorItem: IndicatorItem = MAIndicatorItem()
    private var bottomSelectedIndicatorItem: IndicatorItem = MACDIndicatorItem()

    private var longPress: UILongPressGestureRecognizer?
    private let count = 500

    private var topComposition = DrawableConverterComposition()
    private var bottomComposition = DrawableConverterComposition()
    private var isNeedMoveToEnd = true
    private var timer: Timer?
    public var bottomVC: KLineBottomViewController?
    
    public var checkAction: (() -> Void)? {
        set {
            bottomVC?.checkAction = newValue
        } get {
            bottomVC?.checkAction
        }
    }
    
    public var setAction: (() -> Void)? {
        set {
            bottomVC?.setAction = newValue
        } get {
            bottomVC?.setAction
        }
    }
    
    public var sellAction: (() -> Void)? {
        set {
            bottomVC?.sellAction = newValue
        } get {
            bottomVC?.sellAction
        }
    }

    public var symbol: String = "LLG" {
        didSet {
            if view.window != nil {
                updateKData(symbol: symbol, period: intervalItems[selectedIntervalIndex].id, size: 1000, animated: true)
            }
        }
    }
    
    public var symbolTitle: String = "" {
        didSet {
            if view.window != nil {
                symbolTitleLabel.text = symbolTitle
            }
        }
    }
    
    public var klineShowEntry: Bool = true {
        didSet {
            topComposition.showEntry = klineShowEntry
            refresh()
        }
    }
    
    // MARK: - BottomVC
    public func setFloating(value: Double) {
        bottomVC?.floatingValueLabel.text = String(format: "%.2lf", value)
    }
    
    public func setTotal(value: Double) {
        bottomVC?.totalValueLabel.text = String(format: "%.2lf", value)
    }
    
    public func setAccountValue(value: Double) {
        bottomVC?.accountValueLabel.text = String(format: "%.2lf", value)
    }
    
    public func setGuaranteeRatio(value: Double) {
        bottomVC?.guaranteeRatioValueLabel.text = String(format: "%.2lf", value)
    }
    
    public func setAvailableGuarantee(value: Double) {
        bottomVC?.availableGuaranteeValueLabel.text = String(format: "%.2lf", value)
    }
    
    public func setRemaining(value: Double) {
        bottomVC?.remainingValueLabel.text = String(format: "%.2lf", value)
    }
    
    public var buyValue: Double {
        set {
            bottomVC?.buyValue = newValue
        }
        get {
            bottomVC?.buyValue ?? 0
        }
    }
    
    public var sellValue: Double {
        set {
            bottomVC?.sellValue = newValue
        }
        get {
            bottomVC?.sellValue ?? 0
        }
    }
    
    public var entryPrices: [CGFloat]? {
        didSet {
            refresh()
        }
    }
    public var sellPrices: [CGFloat]? {
        didSet {
            refresh()
        }
    }
    
    public static func newInstance() -> KLineViewController {
        return UIStoryboard(name: "KLine", bundle: Bundle.module).instantiateViewController(withIdentifier: "KLineViewController") as! KLineViewController
    }

    public static func newInstance(domain: String) -> KLineViewController {
        APIService.domain = domain
        return UIStoryboard(name: "KLine", bundle: Bundle.module).instantiateViewController(withIdentifier: "KLineViewController") as! KLineViewController
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        topCanvas.delegate = self
        bottomCanvas.delegate = self

//        let date = Date()
//        var diff = DateComponents()
//        diff.minute = -count * 6
//        let firstDate = Calendar.current.date(byAdding: diff, to: date)!
//        kData = (0..<count).map {
//            let date = Calendar.current.date(byAdding: DateComponents(minute: $0), to: firstDate)!
//            let openPrice = CGFloat.random(in: 0...100) + CGFloat($0 * 10)
//            let closePrice = CGFloat.random(in: 0...100) + CGFloat($0 * 10)
//            let max = max(openPrice, closePrice)
//            let highPrice = max + CGFloat.random(in: 0...100)
//            let min = min(openPrice, closePrice)
//            let lowPrice = min - CGFloat.random(in: 0...100)
//            return KData(openPrice: openPrice, lowPrice: lowPrice, highPrice: highPrice, closePrice: closePrice, timestamp: date.timeIntervalSince1970)
//        }
//        updateCanvas(animated: true)

        updateKData(symbol: symbol, period: intervalItems[selectedIntervalIndex].id, size: 1000, animated: false)
    }

    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        refresh()
    }

    public func refresh() {
        symbolTitleLabel.text = symbolTitle
        topCanvas.refresh()
        bottomCanvas.refresh()
    }

    public func update(newData: [KData]) {
        guard !kData.isEmpty else {
            return
        }
        for data in newData {
            if data.timestamp > kData[kData.count - 1].timestamp {
                kData.append(data)
                if kData.count > 1000 {
                    kData.removeFirst()
                }
            } else if data.timestamp == kData[kData.count - 1].timestamp {
                kData[kData.count - 1] = data
            }
        }

        updateCanvas(animated: false)
    }

    private func setupUI() {
        selectedIntervalIndex = intervalItems.firstIndex(where: { $0.id == UserDefaults.standard.kLineInterval }) ?? 0
        topSelectedIndicatorItem = topItems.first(where: {$0.englishTitle == UserDefaults.standard.mainIndicator }) ?? MAIndicatorItem()
        bottomSelectedIndicatorItem = bottomItems.first(where: {$0.englishTitle == UserDefaults.standard.slaveIndicator }) ?? MACDIndicatorItem()

        topIndicatorLabel.text = topSelectedIndicatorItem.englishTitle
        topTypeLabelView.layer.masksToBounds = true
        topTypeLabelView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        topTypeLabelView.layer.cornerRadius = 6
        let gradient = CAGradientLayer()
        gradient.startPoint = .zero
        gradient.endPoint = CGPoint(x: 1, y: 1)
        gradient.colors = [UIColor(red: 0.96, green: 0.78, blue: 0.16, alpha: 1.00).cgColor,
                           UIColor(red: 0.95, green: 0.67, blue: 0.10, alpha: 1.00).cgColor]
        gradient.frame = topTypeLabelView.bounds
        topTypeLabelView.layer.addSublayer(gradient)

        bottomIndicatorLabel.text = bottomSelectedIndicatorItem.englishTitle
        bottomTypeLabelView.layer.masksToBounds = true
        bottomTypeLabelView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        bottomTypeLabelView.layer.cornerRadius = 6
        let gradient2 = CAGradientLayer()
        gradient2.startPoint = .zero
        gradient2.endPoint = CGPoint(x: 1, y: 1)
        gradient2.colors = [UIColor(red: 0.96, green: 0.78, blue: 0.16, alpha: 1.00).cgColor,
                            UIColor(red: 0.95, green: 0.67, blue: 0.10, alpha: 1.00).cgColor]
        gradient2.frame = bottomTypeLabelView.bounds
        bottomTypeLabelView.layer.addSublayer(gradient2)

        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressAction))
        longPress.delegate = self
        topCanvas.addGestureRecognizer(longPress)
        topCanvas.panGestureRecognizer.require(toFail: longPress)
        self.longPress = longPress

        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        topCanvas.addGestureRecognizer(tap)
        tap.require(toFail: longPress)

        topComposition.scale = initialScale
        bottomComposition.scale = initialScale
        topCanvas.scale = initialScale
        bottomCanvas.scale = initialScale

        topIndicatorPeriodLabel.text = topSelectedIndicatorItem.periodString
        bottomIndicatorPeriodLabel.text = bottomSelectedIndicatorItem.periodString

        if #available(iOS 17.0, *) {
            registerForTraitChanges([UITraitVerticalSizeClass.self]) { (traitEnvironment: Self, previousTraitCollection: UITraitCollection) in
                self.refresh()
            }
        }
        
        let bottom = UIStoryboard(name: "KLine", bundle: Bundle.module).instantiateViewController(withIdentifier: "KLineBottomViewController") as! KLineBottomViewController
        bottom.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bottom.view)
        bottom.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        bottom.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        bottom.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottom.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        addChild(bottom)
        bottom.didMove(toParent: self)
        
        bottomVC = bottom
    }

    @objc
    private func longPressAction(_ gestureRecognizer: UIGestureRecognizer) {
        guard !kData.isEmpty, let topCanvas = topCanvas else {
            return
        }

        let point = gestureRecognizer.location(in: topCanvas)
        if point.x > topCanvas.size.width - rightPadding {
            return
        }

        var focusIndex = Int((topCanvas.contentOffset.x + point.x - contentInset.left / 2) / ((barWidth + spacing) * topCanvas.scale))
        if focusIndex >= kData.count {
            focusIndex = kData.count - 1
        }
        if focusIndex < 0 {
            focusIndex = 0
        }

        topComposition.focusIndex = focusIndex
        topComposition.focusY = kData[focusIndex].closePrice
        bottomComposition.focusIndex = focusIndex
        topCanvas.refresh()
        bottomCanvas.refresh()

        showFocusData(index: focusIndex)
        focusDataView.isHidden = false
    }

    @objc
    private func tapAction(_ gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            hideFocus()
        }
    }


    @IBAction private func showIndicatorVC() {
        let vc = UIStoryboard(name: "ChartChooseVC", bundle: Bundle.module).instantiateViewController(withIdentifier: "ChartChooseVC") as! ChartChooseVC
        vc.topItems = topItems
        vc.bottomItems = bottomItems
        vc.modalPresentationStyle = .overCurrentContext
        vc.topSelectedId = topSelectedIndicatorItem.englishTitle
        vc.bottomSelectedId = bottomSelectedIndicatorItem.englishTitle

        let currentScale: CGFloat = topComposition.scale
        vc.onTopClick = { item in
            UserDefaults.standard.mainIndicator = item.englishTitle
            self.topSelectedIndicatorItem = item
            self.topIndicatorLabel.text = item.englishTitle

            let converters = item.getIndicator(kData: self.kData, barWidth: self.barWidth, spacing: self.spacing)
            converters.forEach {
                $0.contentInset = self.contentInset
            }
            var temp: DrawableConverter? = nil
            if self.topComposition.converters.count > 1 {
                temp = self.topComposition.converters.removeFirst()
            }
            self.topComposition.converters.removeAll()
            if let temp = temp {
                self.topComposition.converters.append(temp)
            }
            self.topComposition.converters.append(contentsOf: converters)
            self.topComposition.scale = currentScale
            self.topIndicatorPeriodLabel.text = self.topSelectedIndicatorItem.periodString
            self.bottomIndicatorPeriodLabel.text = self.bottomSelectedIndicatorItem.periodString
            self.hideFocus()
        }

        vc.onBottomClick = { item in
            UserDefaults.standard.slaveIndicator = item.englishTitle
            self.bottomSelectedIndicatorItem = item
            self.bottomIndicatorLabel.text = item.englishTitle

            let converters = item.getIndicator(kData: self.kData, barWidth: self.barWidth, spacing: self.spacing)
            converters.forEach {
                $0.contentInset = self.contentInset
            }
            self.bottomComposition.converters.removeAll()
            self.bottomComposition.converters.append(contentsOf: converters)
            self.bottomComposition.scale = currentScale
            self.bottomCanvas.refresh()
            self.topIndicatorPeriodLabel.text = self.topSelectedIndicatorItem.periodString
            self.bottomIndicatorPeriodLabel.text = self.bottomSelectedIndicatorItem.periodString
            self.hideFocus()
        }

        present(vc, animated: true)
    }

    private func updateKData(symbol: String, period: String, size: Int, animated: Bool) {
        let request = KLineRequest(symbol: symbol, period: period, size: size)
        request.callback = { response in
            DispatchQueue.main.async {
                self.bidLabel.text = String(format: "%.2lf", response.data.dailySummary.bid)
                self.priceChangeLabel.text = String(format: "%.2lf", response.data.dailySummary.priceChange)
                self.priceChangePercentLabel.text = String(format: "%.2lf%%", response.data.dailySummary.priceChangePercent)

                self.openLabel.text = String(format: "%.2lf", response.data.dailySummary.open)
                self.closeLabel.text = String(format: "%.2lf", response.data.dailySummary.close)
                self.highLabel.text = String(format: "%.2lf", response.data.dailySummary.high)
                self.lowLabel.text = String(format: "%.2lf", response.data.dailySummary.low)

                self.kData = response.data.kLines.map {
                    KData(openPrice: $0.open, lowPrice: $0.low, highPrice: $0.high, closePrice: $0.close, timestamp: $0.timestamp)
                }
                self.updateCanvas(animated: animated)
//                self.scheduleTimer()
                self.dismissLoading()
            }
        }
        request.errorHandler = { i, s in
            DispatchQueue.main.async {
                self.dismissLoading()
            }
        }
        showLoading()
        request.execute()
    }

    private func updateCanvas(animated: Bool) {
        let scale = topComposition.scale
        topComposition.converters.removeAll()
        let converter = KLineDrawableConverter(kData: kData, barWidth: barWidth, spacing: spacing)
        converter.contentInset = contentInset
        topComposition.converters.append(converter)
        let topIndicators = topSelectedIndicatorItem.getIndicator(kData: kData, barWidth: barWidth, spacing: spacing)
        topIndicators.forEach {
            $0.contentInset = contentInset
        }
        topComposition.converters.append(contentsOf: topIndicators)
        topComposition.scale = scale
        topComposition.currentPrice = kData.last?.closePrice
        topComposition.entryPrices = entryPrices
        topComposition.sellPrices = sellPrices

        bottomComposition.converters.removeAll()
        let bottomIndicators = bottomSelectedIndicatorItem.getIndicator(kData: kData, barWidth: barWidth, spacing: spacing)
        bottomIndicators.forEach {
            $0.contentInset = contentInset
        }
        bottomComposition.converters.append(contentsOf: bottomIndicators)
        bottomComposition.scale = scale

        let dataTimes = kData.map {
            CGFloat($0.timestamp)
        }
        let xDrawable = XDrawable(timestamps: dataTimes, spacing: barWidth + spacing, contentInset: contentInset)
        topComposition.xDrawable = xDrawable
        bottomComposition.xDrawable = xDrawable
        topComposition.contentInset = contentInset
        bottomComposition.contentInset = contentInset

        if animated {
            topCanvas.refreshAnimated()
            bottomCanvas.refreshAnimated()
        } else {
            topCanvas.refresh()
            bottomCanvas.refresh()
        }


        if isNeedMoveToEnd {
            isNeedMoveToEnd = false
            topCanvas.contentOffset.x = max(topComposition.contentWidth - view.frame.width, 0)
            bottomCanvas.contentOffset.x = max(topComposition.contentWidth - view.frame.width, 0)
        }

        hideFocus()
    }

    private func showFocusData(index: Int) {
        topIndicatorPeriodLabel.text = topSelectedIndicatorItem.periodString
        bottomIndicatorPeriodLabel.text = bottomSelectedIndicatorItem.periodString
        let data = kData[index]
        focusDataOpenLabel.text = String(format: "%.2lf", data.openPrice)
        focusDataCloseLabel.text = String(format: "%.2lf", data.closePrice)
        focusDataHighLabel.text = String(format: "%.2lf", data.highPrice)
        focusDataLowLabel.text = String(format: "%.2lf", data.lowPrice)
        topIndicatorValueLabel.attributedText = topComposition.getInfo(index: index)
        bottomIndicatorValueLabel.attributedText = bottomComposition.getInfo(index: index)
    }

    private func showLoading() {
        loadingView.isHidden = false
        loadingView.alpha = 0
        activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.25) {
            self.loadingView.alpha = 1
        }
    }
    
    private func dismissLoading() {
        UIView.animate(withDuration: 0.25) {
            self.loadingView.alpha = 0
        } completion: { b in
            self.activityIndicator.stopAnimating()
            self.loadingView.isHidden = true
        }
    }

    private func scheduleTimer() {
        timer?.invalidate()
        let timer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { [weak self] _ in
            guard let self = self else {
                return
            }
            updateKData(symbol: symbol, period: intervalItems[selectedIntervalIndex].id, size: 1000, animated: false)
        }
        self.timer = timer
    }

    private func hideFocus() {
        topComposition.focusIndex = nil
        topComposition.focusY = nil
        bottomComposition.focusIndex = nil
        bottomComposition.focusY = nil
        focusDataView.isHidden = true
        if !kData.isEmpty {
            showFocusData(index: kData.count - 1)
        }
        topCanvas.refresh()
        bottomCanvas.refresh()
    }
}

extension KLineViewController: CanvasDelegate {
    func onScale(canvas: Canvas, scale: CGFloat) {
        longPress?.state = .failed
        topComposition.scale = scale
        bottomComposition.scale = scale
        topCanvas.scale = scale
        bottomCanvas.scale = scale
    }

    func draw(canvas: Canvas, in rect: CGRect, animatingScale: CGFloat) {
        let rect = CGRect(origin: CGPoint(x: rect.origin.x, y: rect.origin.y), size: CGSize(width: rect.width - rightPadding, height: rect.height))
        topComposition.drawTime = true
        bottomComposition.drawTime = false

        if canvas === topCanvas {
            topComposition.draw(canvas: canvas, rect: rect, animatingScale: animatingScale)
        } else {
            bottomComposition.draw(canvas: canvas, rect: rect, animatingScale: animatingScale)
        }
    }

    func contentSize(canvas: Canvas) -> CGSize {
        let maxWidth = max(topComposition.contentWidth, bottomComposition.contentWidth)
        if canvas === topCanvas {
            return CGSize(width: maxWidth, height: canvas.size.height)
        } else {
            return CGSize(width: maxWidth, height: canvas.size.height)
        }
    }

    func onScroll(canvas: Canvas, contentOffset: CGPoint) {
        if canvas === topCanvas {
            bottomCanvas.contentOffset = CGPoint(x: contentOffset.x, y: 0)
        } else {
            topCanvas.contentOffset = CGPoint(x: contentOffset.x, y: 0)
        }
    }

    func scrollViewWillBeginDragging(canvas: Canvas) {
        hideFocus()
    }
}

extension KLineViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        intervalItems.count
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KLineTimeUnitCell", for: indexPath) as! KLineTimeUnitCell
        cell.titleLabel.text = intervalItems[indexPath.item].title
        if indexPath.item == selectedIntervalIndex {
            cell.contentView.backgroundColor = UIColor(red: 0.99, green: 0.85, blue: 0.44, alpha: 1.00)
        } else {
            cell.contentView.backgroundColor = .clear
        }
        return cell
    }

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIntervalIndex = indexPath.item
        UserDefaults.standard.kLineInterval = intervalItems[indexPath.item].id
        collectionView.reloadData()
        isNeedMoveToEnd = true
        updateKData(symbol: symbol, period: intervalItems[indexPath.item].id, size: 1000, animated:  true)
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: 40, height: collectionView.bounds.height)
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        4
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }
}

extension KLineViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
}

class MarketGradientView: UIView {
    override class var layerClass: AnyClass {
        CAGradientLayer.self
    }

    required init?(coder: NSCoder) {

        super.init(coder: coder)
        guard let gradient = layer as? CAGradientLayer else {
            return
        }
        gradient.startPoint = .zero
        gradient.endPoint = CGPoint(x: 1, y: 1)
        gradient.colors = [UIColor(red: 0.96, green: 0.78, blue: 0.16, alpha: 1.00).cgColor,
                           UIColor(red: 0.95, green: 0.67, blue: 0.10, alpha: 1.00).cgColor]
    }
}

class KLineTimeUnitCell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!
}

extension UserDefaults {
    private var kLineIntervalKey: String { "kLineViewController.interval" }
    private var mainIndicatorKey: String { "kLineViewController.mainIndicator" }
    private var slaveIndicatorKey: String { "kLineViewController.slaveIndicator" }

    var kLineInterval: String {
        get {
            UserDefaults.standard.string(forKey: kLineIntervalKey) ?? "1d"
        }
        set {
            UserDefaults.standard.set(newValue, forKey: kLineIntervalKey)
        }
    }

    var mainIndicator: String {
        get {
            UserDefaults.standard.string(forKey: mainIndicatorKey) ?? "MA"
        }
        set {
            UserDefaults.standard.set(newValue, forKey: mainIndicatorKey)
        }
    }

    var slaveIndicator: String {
        get {
            UserDefaults.standard.string(forKey: slaveIndicatorKey) ?? "MACD"
        }
        set {
            UserDefaults.standard.set(newValue, forKey: slaveIndicatorKey)
        }
    }
}
