//
//  File.swift
//  
//
//  Created by Tony Wu on 2023/11/28.
//

import UIKit

public class KLineBottomViewController: UIViewController {
    @IBOutlet public var floatingTitleLabel: UILabel!
    @IBOutlet public var totalTitleLabel: UILabel!
    @IBOutlet public var accountValueTitleLabel: UILabel!
    @IBOutlet public var guaranteeRatioTitleLabel: UILabel!
    @IBOutlet public var availableGuaranteeTitleLabel: UILabel!
    @IBOutlet public var remainingTitleLabel: UILabel!
    @IBOutlet public var checkButton: UIButton!
    
    @IBOutlet var floatingValueLabel: UILabel!
    @IBOutlet var totalValueLabel: UILabel!
    @IBOutlet var accountValueLabel: UILabel!
    @IBOutlet var guaranteeRatioValueLabel: UILabel!
    @IBOutlet var availableGuaranteeValueLabel: UILabel!
    @IBOutlet var remainingValueLabel: UILabel!
    
    
    @IBOutlet var buyButton: UIButton!
    @IBOutlet var sellButton: UIButton!
    @IBOutlet var bottomBackgroundView: UIView!
    @IBOutlet var showOwnBackgroundView: UIView!
    @IBOutlet var gridBackgroundView: UIView!
    @IBOutlet var gridMinXMinYView: UIView!
    @IBOutlet var gridMaxXMinYView: UIView!
    @IBOutlet var gridMinXMaxYView: UIView!
    @IBOutlet var gridMaxXMaxYView: UIView!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var pathView: UIView!
    
    @IBOutlet var infoStackView: UIStackView!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var openButton: UIButton!
    
    private let shape = CAShapeLayer()
    private let color = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.00)
    private let scale: CGFloat = 1.2
    
    private let blue = UIColor(red: 0.31, green: 0.58, blue: 0.89, alpha: 1.00)
    private let red = UIColor(red: 1.00, green: 0.42, blue: 0.38, alpha: 1.00)
    private let gray = UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1.00)
    private let shadowOffset: CGFloat = 2
    private let radius: CGFloat = 80
    private let tapView = UIView()
    
    var checkAction: (() -> Void)?
    var setAction: (() -> Void)?
    var sellAction: (() -> Void)?
    
    var buyValue: Double = 0 {
        didSet {
            let title = NSLocalizedString("buy_in", comment: "")
            buyButton.setTitle(String(format: "%@ %.2lf", title, buyValue), for: .normal)
        }
    }
    
    var sellValue: Double = 0 {
        didSet {
            let title = NSLocalizedString("sell_out", comment: "")
            sellButton.setTitle(String(format: "%@ %.2lf", title, sellValue), for: .normal)
        }
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        floatingTitleLabel.text = NSLocalizedString("floating_profit_and_loss", comment: "")
        totalTitleLabel.text = NSLocalizedString("total_lots", comment: "test")
        accountValueTitleLabel.text = NSLocalizedString("account_equity", comment: "")
        guaranteeRatioTitleLabel.text = NSLocalizedString("margin_ratio", comment: "")
        availableGuaranteeTitleLabel.text = NSLocalizedString("available_margin", comment: "")
        remainingTitleLabel.text = NSLocalizedString("remain_amount", comment: "")
        checkButton.setTitle(NSLocalizedString("click_to_see_all_positions", comment: ""), for: .normal)
        checkButton.setTitleColor(UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1.00), for: .normal)

        buyButton.layer.cornerRadius = 4
        sellButton.layer.cornerRadius = 4
        showOwnBackgroundView.layer.cornerRadius = 4
        bottomBackgroundView.layer.cornerRadius = 8
        bottomBackgroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        gridBackgroundView.layer.cornerRadius = 8
        
        gridMinXMinYView.layer.maskedCorners = [.layerMinXMinYCorner]
        gridMinXMinYView.layer.cornerRadius = 8
        
        gridMaxXMinYView.layer.maskedCorners = [.layerMaxXMinYCorner]
        gridMaxXMinYView.layer.cornerRadius = 8
        
        gridMinXMaxYView.layer.maskedCorners = [.layerMinXMaxYCorner]
        gridMinXMaxYView.layer.cornerRadius = 8
        
        gridMaxXMaxYView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        gridMaxXMaxYView.layer.cornerRadius = 8
        
        closeButton.alpha = 0
        bottomConstraint.constant = -136
        
        pathView.layer.shadowRadius = 2
        pathView.layer.shadowOpacity = 0.3
        pathView.layer.shadowOffset = CGSize(width: 0, height: -shadowOffset)
        
        totalValueLabel.textColor = gray
        accountValueLabel.textColor = red
        guaranteeRatioValueLabel.textColor = blue
        availableGuaranteeValueLabel.textColor = gray
        remainingValueLabel.textColor = gray
        
        shape.fillColor = color.cgColor
        shape.transform = CATransform3DScale(shape.transform , scale, 1, 1)
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: radius))
        path.addLine(to: CGPoint(x: view.center.x - radius / 2, y: radius + shadowOffset * 2))
        path.addArc(withCenter: CGPoint(x: view.center.x, y: radius + shadowOffset * 2), radius: radius, startAngle: -.pi, endAngle: 0, clockwise: true)
        path.close()
        shape.path = path.cgPath
        shape.frame = view.bounds.offsetBy(dx: -view.bounds.width / 2 * (scale - 1), dy: 0)
        pathView.layer.addSublayer(shape)
        pathView.bringSubviewToFront(pathView.subviews.first!)
        
        tapView.isHidden = true
        tapView.backgroundColor = .clear
        tapView.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(tapView, belowSubview: pathView)
        tapView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tapView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tapView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tapView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(hide))
        tapView.addGestureRecognizer(tap)
    }
    
    @IBAction private func show() {
        bottomConstraint.constant = 0
        tapView.isHidden = false
        UIView.animate(withDuration: 0.25) {
            self.openButton.alpha = 0
            self.closeButton.alpha = 1
            self.view.backgroundColor = .black.withAlphaComponent(82/255)
            self.view.layoutSubviews()
        }
    }
    
    @IBAction private func hide() {
        tapView.isHidden = true
        bottomConstraint.constant = -136
        UIView.animate(withDuration: 0.25) {
            self.openButton.alpha = 1
            self.closeButton.alpha = 0
            self.view.backgroundColor = .black.withAlphaComponent(0)
            self.view.layoutSubviews()
        }
    }
    
    func setFloating(value: Double) {
        floatingValueLabel.text = String(format: "%.2lf", value)
        floatingTitleLabel.textColor = value >= 0 ? blue : red
    }
    
    func setTotal(value: Double) {
        totalValueLabel.text = String(format: "%.2lf", value)
    }
    
    func setAccount(value: Double) {
        accountValueLabel.text = String(format: "%.2lf", value)
    }
    
    func setGuranteeRatio(value: Double) {
        guaranteeRatioValueLabel.text = String(format: "%.2lf", value)
    }
    
    func setAvailableGuarantee(value: Double) {
        availableGuaranteeValueLabel.text = String(format: "%.2lf", value)
    }
    
    func setRemaining(value: Double) {
        remainingValueLabel.text = String(format: "%.2lf", value)
    }
    
    @IBAction private func checkButtnAction() {
        checkAction?()
    }
}


class KLineButtomViewBackgroundView: UIView {
    private let path = UIBezierPath()
    private let radius: CGFloat = 80
    private let shape = CAShapeLayer()
    private let color = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.00)
    private let scale: CGFloat = 1.2
    private let shadowOffset: CGFloat = 2
    required init?(coder: NSCoder) {
        super.init(coder: coder)

        layer.masksToBounds = true
        shape.fillColor = color.cgColor
        shape.transform = CATransform3DScale(shape.transform , scale, 1, 1)
        layer.addSublayer(shape)
        
        layer.shadowRadius = 2
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0, height: -shadowOffset)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        path.move(to: CGPoint(x: 0, y: radius))
        path.addLine(to: CGPoint(x: center.x - radius / 2, y: radius + shadowOffset * 2))
        path.addArc(withCenter: CGPoint(x: center.x, y: radius + shadowOffset * 2), radius: radius, startAngle: -.pi, endAngle: 0, clockwise: true)
        path.close()
        shape.path = path.cgPath
        shape.frame = bounds.offsetBy(dx: -bounds.width / 2 * (scale - 1), dy: 0)
    }
}

class SubView: UIView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if super.hitTest(point, with: event) == self {
            return nil
        }
        return super.hitTest(point, with: event)
    }
}

class ResponseButton: UIButton {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        var heightExpansion: CGFloat = 0
        var widthExpansion: CGFloat = 0
        if bounds.height < 44 {
            heightExpansion = (bounds.height - 44) / CGFloat(2)
        }
        if bounds.width < 44 {
            widthExpansion = (bounds.width - 44) / CGFloat(2)
        }
        
        return bounds.inset(by: UIEdgeInsets(top: heightExpansion, left: widthExpansion, bottom: heightExpansion, right: widthExpansion)).contains(point)
    }
}

