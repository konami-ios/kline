//
//  IndicatorItem.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/12.
//

import Foundation

protocol IndicatorItem {
    var chineseTitle: String { get }
    var englishTitle: String { get }
    var periodString: String { get }
    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter]
}
