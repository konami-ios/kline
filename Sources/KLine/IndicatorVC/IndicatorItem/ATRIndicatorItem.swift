//
// Created by Tony Wu on 2023/10/13.
//

import Foundation

class ATRIndicatorItem: IndicatorItem {
    let chineseTitle: String = "平均波幅"
    let englishTitle: String = "ATR"
    let periodString: String = "(14)"

    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter] {
        [ATRDrawableConverter(kData: kData, period: 14, barWidth: barWidth, spacing: spacing, color: .blue),
         MTRDrawableConverter(kData: kData, period: 1, barWidth: barWidth, spacing: spacing, color: .red)]
    }
}