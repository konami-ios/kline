//
// Created by Tony Wu on 2023/10/13.
//

import Foundation

class RSIIndicatorItem: IndicatorItem {
    let chineseTitle: String = "相对强弱"
    let englishTitle: String = "RSI"
    let periodString: String = "(6, 12, 24)"
    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter] {
        [RSIDrawableConverter(kData: kData, period: 6, barWidth: barWidth, spacing: spacing, color: .blue),
         RSIDrawableConverter(kData: kData, period: 12, barWidth: barWidth, spacing: spacing, color: .red),
         RSIDrawableConverter(kData: kData, period: 24, barWidth: barWidth, spacing: spacing, color: .brown)]
    }
}