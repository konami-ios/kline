//
//  MACDIndicatorItem.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/12.
//

import Foundation

class MACDIndicatorItem: IndicatorItem {
    let chineseTitle: String = "多空指标"
    let englishTitle: String = "MACD"
    let periodString: String = "(12, 26, 9)"
    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter] {
        [MACDDrawableConverter(kData: kData, barWidth: barWidth, spacing: spacing)]
    }
}
