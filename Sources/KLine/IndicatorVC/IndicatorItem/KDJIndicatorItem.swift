//
// Created by Tony Wu on 2023/10/13.
//

import Foundation

class KDJIndicatorItem: IndicatorItem {
    let chineseTitle: String = "随机指标"
    let englishTitle: String = "KDJ"
    let periodString: String = "(9, 3, 3)"
    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter] {
        [KDJDrawableConverter(kData: kData, period: 9, barWidth: barWidth, spacing: spacing, color: .blue, type: .k),
         KDJDrawableConverter(kData: kData, period: 9, barWidth: barWidth, spacing: spacing, color: .red, type: .d),
         KDJDrawableConverter(kData: kData, period: 9, barWidth: barWidth, spacing: spacing, color: .green, type: .j)]
    }
}