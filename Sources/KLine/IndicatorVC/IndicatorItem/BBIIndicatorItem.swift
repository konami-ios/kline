//
// Created by Tony Wu on 2023/10/13.
//

import Foundation

class BBIIndicatorItem: IndicatorItem {
    let chineseTitle: String = "多空指標"
    let englishTitle: String = "BBI"
    let periodString: String = "(3, 6, 12, 24)"
    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter] {
        [BBIDrawableConverter(kData: kData, period: 24, barWidth: barWidth, spacing: spacing, color: .blue)]
    }
}