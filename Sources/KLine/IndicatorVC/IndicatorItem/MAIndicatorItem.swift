//
//  MAIndicatorItem.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/12.
//

import Foundation

class MAIndicatorItem: IndicatorItem {
    let chineseTitle: String = "移动平均线"
    let englishTitle: String = "MA"
    let periodString: String = "(5, 10, 20, 60)"

    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter] {
        [MADrawableConverter(kData: kData, period: 5, barWidth: barWidth, spacing: spacing, color: .blue),
         MADrawableConverter(kData: kData, period: 10, barWidth: barWidth, spacing: spacing, color: .red),
         MADrawableConverter(kData: kData, period: 20, barWidth: barWidth, spacing: spacing, color: .green),
         MADrawableConverter(kData: kData, period: 60, barWidth: barWidth, spacing: spacing, color: .black)]
    }
}