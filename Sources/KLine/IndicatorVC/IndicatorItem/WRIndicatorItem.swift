//
// Created by Tony Wu on 2023/10/13.
//

import Foundation

class WRIndicatorItem: IndicatorItem {
    let chineseTitle: String = "多空强弱"
    let englishTitle: String = "WR"
    let periodString: String = "(14)"
    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter] {
        [WRDrawableConverter(kData: kData, period: 1, barWidth: barWidth, spacing: spacing, color: .brown)]
    }
}