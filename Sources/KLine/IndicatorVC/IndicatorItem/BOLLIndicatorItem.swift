//
//  BOLLIndicatorItem.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/12.
//

import Foundation

class BOLLIndicatorItem: IndicatorItem {
    let chineseTitle: String = "布林线"
    let englishTitle: String = "BOLL"
    let periodString: String = "(20, 2)"
    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter] {
        [BollDrawableConverter(kData: kData, period: 20, barWidth: barWidth, spacing: spacing, color: .blue, type: .up),
         BollDrawableConverter(kData: kData, period: 20, barWidth: barWidth, spacing: spacing, color: .red, type: .ma),
         BollDrawableConverter(kData: kData, period: 20, barWidth: barWidth, spacing: spacing, color: .black, type: .dn)]
    }
}
