//
// Created by Tony Wu on 2023/10/13.
//

import Foundation

class KDIndicatorItem: IndicatorItem {
    let chineseTitle: String = "随机震荡"
    let englishTitle: String = "KD"
    let periodString: String = "(9, 3, 3)"
    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter] {
        [KDJDrawableConverter(kData: kData, period: 9, barWidth: barWidth, spacing: spacing, color: .blue, type: .k),
         KDJDrawableConverter(kData: kData, period: 9, barWidth: barWidth, spacing: spacing, color: .red, type: .d)]
    }
}