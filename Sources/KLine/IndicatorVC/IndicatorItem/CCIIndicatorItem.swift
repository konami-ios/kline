//
// Created by Tony Wu on 2023/10/13.
//

import Foundation

class CCIIndicatorItem: IndicatorItem {
    let chineseTitle: String = "顺势指标"
    let englishTitle: String = "CCI"
    let periodString: String = "(14)"
    func getIndicator(kData: [KData], barWidth: CGFloat, spacing: CGFloat) -> [DrawableConverter] {
        [CCIDrawableConverter(kData: kData, period: 1, barWidth: barWidth, spacing: spacing, color: .blue)]
    }
}