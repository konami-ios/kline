//
//  ChartChooseVC.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/12.
//

import Foundation
import UIKit

class ChartChooseVC: UIViewController {
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var confirmButton: UIButton!
    @IBOutlet var setupImageView: UIImageView!
    var topItems: [IndicatorItem] = []//[MAIndicatorItem(), BOLLIndicatorItem(), BBIIndicatorItem()]
    var bottomItems: [IndicatorItem] = []//[MACDIndicatorItem(), RSIIndicatorItem(), KDJIndicatorItem(), WRIndicatorItem(), ATRIndicatorItem(), CCIIndicatorItem(), KDIndicatorItem()]

    var topSelectedId: String = ""
    var bottomSelectedId: String = ""
    var onTopClick: ((IndicatorItem) -> Void)?
    var onBottomClick: ((IndicatorItem) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()

        let layout = makeLayout()
        collectionView.collectionViewLayout = layout
    }

    private func setupUI() {
        containerView.layer.cornerRadius = 4
        containerView.layer.masksToBounds = true

        confirmButton.layer.cornerRadius = 3.3
//        setupImageView.image = UIImage(named: "icon_market_setting", in: Bundle.module, with: nil)
//        let image = UIImage(named: "icon_market_setting", in: Bundle.module, with: nil)
//        print("image = \(image)")
//         collectionView.backgroundColor = collectionView.backgroundColor?.withAlphaComponent(0.5)
//        containerView.backgroundColor = containerView.backgroundColor?.withAlphaComponent(0.5)

//        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
//        view.addGestureRecognizer(tap)
    }

    @objc
    private func tapAction(_ gesture: UITapGestureRecognizer) {
        dismiss(animated: true)
    }

    @IBAction func confirmAction() {
        dismiss(animated: true)
    }
}

extension ChartChooseVC: UICollectionViewDataSource, UICollectionViewDelegate {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        2
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return topItems.count
        } else if section == 1 {
            return bottomItems.count
        }

        return 0
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let returnCell: UICollectionViewCell
        if indexPath.section == 0 {
            if topSelectedId == topItems[indexPath.item].englishTitle {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChartChooseHighlightCollectionViewCell", for: indexPath) as! ChartChooseHighlightCollectionViewCell
                cell.titleLabel.text = topItems[indexPath.item].englishTitle + topItems[indexPath.item].chineseTitle
                returnCell = cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChartChooseCollectionViewCell", for: indexPath) as! ChartChooseCollectionViewCell
                cell.titleLabel.text = topItems[indexPath.item].englishTitle + topItems[indexPath.item].chineseTitle
                returnCell = cell
            }
        } else if indexPath.section == 1 {
            if bottomSelectedId == bottomItems[indexPath.item].englishTitle {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChartChooseHighlightCollectionViewCell", for: indexPath) as! ChartChooseHighlightCollectionViewCell
                cell.titleLabel.text = bottomItems[indexPath.item].englishTitle + bottomItems[indexPath.item].chineseTitle
                returnCell = cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChartChooseCollectionViewCell", for: indexPath) as! ChartChooseCollectionViewCell
                cell.titleLabel.text = bottomItems[indexPath.item].englishTitle + bottomItems[indexPath.item].chineseTitle
                returnCell = cell
            }
        } else {
            returnCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChartChooseCollectionViewCell", for: indexPath) as! ChartChooseCollectionViewCell
        }

        return returnCell
    }

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            topSelectedId = topItems[indexPath.item].englishTitle
            onTopClick?(topItems[indexPath.item])
        } else {
            bottomSelectedId = bottomItems[indexPath.item].englishTitle
            onBottomClick?(bottomItems[indexPath.item])
        }
        collectionView.reloadData()
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ChartChooseCollectionViewHeaderCell", for: indexPath) as! ChartChooseCollectionViewHeaderCell
        if indexPath.section == 0 {
            header.topConstraint.constant = 0
            header.titleLabel.text = "主图指标"
        } else {
            header.topConstraint.constant = 20
            header.titleLabel.text = "副图指标"
        }
        return header
    }

    func makeLayout() -> UICollectionViewCompositionalLayout {
        .init { (i: Int, v: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            let itemSize = NSCollectionLayoutSize(widthDimension: .absolute((v.container.contentSize.width - 20) / 2), heightDimension: .fractionalHeight(1))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(30))
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
            group.interItemSpacing = NSCollectionLayoutSpacing.fixed(20)
            let section = NSCollectionLayoutSection(group: group)
            section.boundarySupplementaryItems = [
                .init(layoutSize: .init(widthDimension: .fractionalWidth(1),
                        heightDimension: .estimated(60)),
                        elementKind: UICollectionView.elementKindSectionHeader,
                        alignment: .top)
            ]
            section.interGroupSpacing = 10
            return section
        }
    }
}

class ChartChooseCollectionViewCell: UICollectionViewCell {
    @IBOutlet var borderView: UIView!
    @IBOutlet var titleLabel: UILabel!

    override func awakeFromNib() {
        borderView.backgroundColor = .clear
        borderView.layer.borderWidth = 0.5
        borderView.layer.cornerRadius = 3.3
        borderView.layer.borderColor = UIColor(red: 0.44, green: 0.44, blue: 0.44, alpha: 1.00).cgColor
    }
}

class ChartChooseHighlightCollectionViewCell: UICollectionViewCell {
    @IBOutlet var colorView: UIView!
    @IBOutlet var titleLabel: UILabel!

    override func awakeFromNib() {
        colorView.layer.cornerRadius = 3.3
    }
}


class ChartChooseCollectionViewHeaderCell: UICollectionReusableView {
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var titleLabel: UILabel!
}
