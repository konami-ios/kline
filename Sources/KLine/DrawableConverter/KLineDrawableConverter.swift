//
//  KLineDataConverter.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/3.
//

import Foundation
import UIKit

class KLineDrawableConverter: DrawableConverter {
    private let kData: [KData]
    private let barWidth: CGFloat
    private let spacing: CGFloat

    override var contentWidth: CGFloat {
        (barWidth + spacing) * scale * CGFloat(kData.count) + contentInset.left + contentInset.right
    }

    init(kData: [KData], barWidth: CGFloat, spacing: CGFloat) {
        self.kData = kData
        self.barWidth = barWidth
        self.spacing = spacing
    }

    override func draw(canvas: Canvas, in rect: CGRect, offsetY: CGFloat, yRatio: CGFloat, animatingScale: CGFloat) {
        guard let (start, end) = indexRangeIn(rect: rect) else {
            return
        }
        canvas.save()
        canvas.clip(to: rect)
        (start...end).forEach {
            let color: UIColor
            if kData[$0].closePrice >= kData[$0].openPrice {
                color = UIColor(red: 1.00, green: 0.41, blue: 0.38, alpha: 1.00)
            } else {
                color = UIColor(red: 0.00, green: 0.74, blue: 0.60, alpha: 1.00)
            }

            let x = CGFloat($0) * (barWidth + spacing) * scale +  contentInset.left

            let open = convertY(kData[$0].openPrice, offsetY: offsetY, yRatio: yRatio)
            let close = convertY(kData[$0].closePrice, offsetY: offsetY, yRatio: yRatio)
            let low = convertY(kData[$0].lowPrice, offsetY: offsetY, yRatio: yRatio)
            let high = convertY(kData[$0].highPrice, offsetY: offsetY, yRatio: yRatio)
            let middle = (max(open, close, low, high) + min(open, close, low, high)) / 2
            let candle =  CandleDrawable(
                    openPrice: animatingScale * open + (1 - animatingScale) * middle,
                    lowPrice: animatingScale * low + (1 - animatingScale) * middle,
                    highPrice: animatingScale * high + (1 - animatingScale) * middle,
                    closePrice: animatingScale * close + (1 - animatingScale) * middle,
                    volume: 0,
                    x: x,
                    lineWidth: barWidth * scale,
                    color: color)
            candle.draw(canvas: canvas)
        }
        canvas.restore()
    }

    private func convertY(_ y: CGFloat, offsetY: CGFloat, yRatio: CGFloat) -> CGFloat {
        let value = (y - offsetY) * yRatio + contentInset.bottom
        return value
    }

    override func yRangeIn(rect: CGRect) -> (min: CGFloat, max: CGFloat)? {
        guard let (start, end) = indexRangeIn(rect: rect) else {
            return nil
        }
        let filterDataYs: [(min: CGFloat, max: CGFloat)] = kData[start...end].map { (min($0.openPrice, $0.closePrice, $0.lowPrice, $0.highPrice), max($0.openPrice, $0.closePrice, $0.lowPrice, $0.highPrice)) }

        guard let minValue = (filterDataYs.min { $0.min < $1.min })?.min,
              let maxValue = (filterDataYs.max { $0.max < $1.max })?.max else {
            return nil
        }
        return (minValue, maxValue)
    }

    private func indexRangeIn(rect: CGRect) -> (start: Int, end: Int)? {
        let unit = (barWidth + spacing) * scale
        let start = max(0, Int((rect.minX - contentInset.left / 2) / unit))
        let end = min(Int((rect.maxX - contentInset.left / 2) / unit), kData.count - 1)
        guard end < kData.count, start <= end, end >= 0, start >= 0 else {
            return nil
        }

        return (start, end)
    }
}
