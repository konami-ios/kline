//
// Created by Tony Wu on 2023/10/13.
//

import Foundation
import UIKit

class RSIDrawableConverter: PeriodDrawableConverter {
    override func getPoint(kData: [KData], period: Int) -> [CGFloat] {
        if kData.count < period || period < 1 {
            return []
        }
        return calculateCultersRSI(kDatas: kData, period: period)
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        guard let string = super.getInfo(index: index) else {
            return nil
        }

        let result = NSMutableAttributedString(string: "RSI:", attributes: string.attributes(at: 0, effectiveRange: nil))
        result.append(string)

        return result
    }
}

extension RSIDrawableConverter {
    func calculateCultersRSI(kDatas: [KData], period: Int) -> [CGFloat] {
        var culterRSI = [CGFloat]()

        if kDatas.count < period {
            return culterRSI
        }

        var averageGain = 0.0
        var averageLoss = 0.0

        for i in 1...period {
            let priceChange = kDatas[i].closePrice - kDatas[i - 1].closePrice
            if priceChange > 0 {
                averageGain += priceChange
            } else {
                averageLoss += abs(priceChange)
            }
        }

        averageGain /= CGFloat(period)
        averageLoss /= CGFloat(period)

        culterRSI.append(100 - (100 / (1 + averageGain / averageLoss)))

        for i in period..<kDatas.count {
            let priceChange = kDatas[i].closePrice - kDatas[i - 1].closePrice
            let gain = (priceChange > 0) ? priceChange : 0
            let loss = (priceChange < 0) ? abs(priceChange) : 0

            averageGain = (averageGain * CGFloat(period - 1) + gain) / CGFloat(period)
            averageLoss = (averageLoss * CGFloat(period - 1) + loss) / CGFloat(period)

            culterRSI.append(100 - (100 / (1 + averageGain / averageLoss)))
        }

        return culterRSI
    }
}
