//
// Created by Tony Wu on 2023/10/16.
//

import Foundation
import UIKit

class ATRDrawableConverter: PeriodDrawableConverter {
    override func getPoint(kData: [KData], period: Int) -> [CGFloat] {
        var atrValues = [CGFloat]()
        let mtrPoints = calculateMTR(data: kData)
        guard period - 1 <= mtrPoints.count else {
            return []
        }
        for i in period - 1 ..< mtrPoints.count {
            let atr = mtrPoints[i - period + 1...i].reduce(0.0, +) / CGFloat(period)
            atrValues.append(atr)
        }
        return atrValues
    }

    private func calculateMTR(data: [KData]) -> [CGFloat] {
        var mtrValues = [CGFloat]()

        for i in 0..<data.count {
            let currentData = data[i]

            if i > 0 {
                let previousData = data[i - 1]

                let trueRange = max(currentData.highPrice - currentData.lowPrice,
                        abs(currentData.highPrice - previousData.closePrice),
                        abs(currentData.lowPrice - previousData.closePrice))

                mtrValues.append(trueRange)
            } else {
                mtrValues.append(0)
            }
        }

        return mtrValues
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        guard let string = super.getInfo(index: index) else {
            return nil
        }

        let result = NSMutableAttributedString(string: "ATR:", attributes: string.attributes(at: 0, effectiveRange: nil))
        result.append(string)

        return result
    }
}

class MTRDrawableConverter: PeriodDrawableConverter {
    override func getPoint(kData: [KData], period: Int) -> [CGFloat] {
        calculateMTR(data: kData)
    }

    private func calculateMTR(data: [KData]) -> [CGFloat] {
        var mtrValues = [CGFloat]()

        for i in 0..<data.count {
            let currentData = data[i]

            if i > 0 {
                let previousData = data[i - 1]

                let trueRange = max(currentData.highPrice - currentData.lowPrice,
                        abs(currentData.highPrice - previousData.closePrice),
                        abs(currentData.lowPrice - previousData.closePrice))

                mtrValues.append(trueRange)
            } else {
                mtrValues.append(0)
            }
        }

        return mtrValues
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        guard let string = super.getInfo(index: index) else {
            return nil
        }

        let result = NSMutableAttributedString(string: "MTR:", attributes: string.attributes(at: 0, effectiveRange: nil))
        result.append(string)

        return result
    }
}