//
// Created by Tony Wu on 2023/10/24.
//

import Foundation
import UIKit
class DrawableConverterComposition: DrawableConverter {
    var converters = [DrawableConverter]()
    var focusIndex: Int?
    var focusY: CGFloat?
    var currentPrice: CGFloat?
    var entryPrices: [CGFloat]?
    var sellPrices: [CGFloat]?
    var drawTime: Bool = true
    var xDrawable: XDrawable?
    var showEntry = false
    override var scale: CGFloat {
        didSet {
            converters.forEach { $0.scale = scale }
        }
    }
    override var contentInset: UIEdgeInsets {
        didSet {
            converters.forEach {
                $0.contentInset = contentInset
            }
        }
    }
    override var contentWidth: CGFloat {
        (converters.map { $0.contentWidth }.max { $0 < $1 } ?? 0)
    }

    override func yRangeIn(rect: CGRect) -> (min: CGFloat, max: CGFloat)? {
        let otherRanges: [(min: CGFloat, max: CGFloat)] = converters.compactMap {
            $0.yRangeIn(rect: rect)
        }

        guard let finalMin = (otherRanges.min { $0.min < $1.min })?.min,
              let finalMax = (otherRanges.max { $0.max < $1.max })?.max else {
            return nil
        }
//        return (finalMin, finalMax)
        let marginDiff = (finalMax - finalMin) * 0.05
        return (finalMin - marginDiff, finalMax + marginDiff)
    }

    func draw(canvas: Canvas, rect: CGRect, animatingScale : CGFloat) {
        guard let yRange = yRangeIn(rect: rect) else {
            return
        }
        let ratio: CGFloat
        if yRange.min == yRange.max {
            ratio = 1
        } else {
            ratio = (rect.height - contentInset.top - contentInset.bottom) / abs(yRange.max - yRange.min)
        }

        let yDrawable = YDrawable(min: yRange.min, max: yRange.max, targetRect: rect, contentInset: contentInset, focusY: focusY)
        yDrawable.currentPrice = currentPrice
        yDrawable.entryPrices = entryPrices
        yDrawable.sellPrices = sellPrices
        yDrawable.showEntry = showEntry
        yDrawable.draw(canvas: canvas)

        xDrawable?.focusIndex = focusIndex
        xDrawable?.drawTime = drawTime
        xDrawable?.draw(canvas: canvas)


        converters.forEach {
            $0.draw(canvas: canvas, in: rect, offsetY: yRange.min, yRatio: ratio, animatingScale: animatingScale)
        }
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        let result = NSMutableAttributedString(string: "")
        return converters.reduce(result) {
            if let s = $1.getInfo(index: index) {
                $0.append(s)
            }
            return $0
        }
    }
}
