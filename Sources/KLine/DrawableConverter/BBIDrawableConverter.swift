//
// Created by Tony Wu on 2023/10/13.
//

import Foundation
import UIKit

class BBIDrawableConverter: PeriodDrawableConverter {
    private let shortPeriod = 3
    private let middlePeriod = 6
    private let longPeriod = 12
    private let veryLongPeriod = 24

    override func getPoint(kData: [KData], period: Int) -> [CGFloat] {
        var bbiValues: [CGFloat] = []

        if kData.count < longPeriod {
            return bbiValues
        }

        for i in (veryLongPeriod - 1)..<kData.count {
            var shortMA: Double = 0.0
            var middleMA: Double = 0.0
            var longMA: Double = 0.0
            var veryLongMA: Double = 0.0

            if i >= shortPeriod - 1 {
                shortMA = calculateMovingAverage(data: Array(kData[(i - shortPeriod + 1)...i]))
            }

            if i >= middlePeriod - 1 {
                middleMA = calculateMovingAverage(data: Array(kData[(i - middlePeriod + 1)...i]))
            }

            if i >= longPeriod - 1 {
                longMA = calculateMovingAverage(data: Array(kData[(i - longPeriod + 1)...i]))
            }

            if i >= veryLongPeriod - 1 {
                veryLongMA = calculateMovingAverage(data: Array(kData[(i - veryLongPeriod + 1)...i]))
                let bbi = (shortMA + middleMA + longMA + veryLongMA) / 4.0
                bbiValues.append(bbi)
            }
        }
        return bbiValues
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        guard let string = super.getInfo(index: index) else {
            return nil
        }
        
        let result = NSMutableAttributedString(string: "BBI:", attributes: string.attributes(at: 0, effectiveRange: nil))
        result.append(string)

        return result
    }

    private func calculateMovingAverage(data: [KData]) -> Double {
        let sum = data.reduce(0.0) { $0 + $1.closePrice }
        return sum / Double(data.count)
    }
}
