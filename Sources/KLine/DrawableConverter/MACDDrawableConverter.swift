//
//  MACDDrawableConverter.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/11.
//

import Foundation
import UIKit

class MACDDrawableConverter: DrawableConverter {
    override var contentWidth: CGFloat {
        (barWidth + spacing) * scale * CGFloat(kData.count)
    }

    private let difColor: UIColor = .blue
    private let deaColor: UIColor = .red
    private let positiveColor = UIColor(red: 1.00, green: 0.41, blue: 0.38, alpha: 1.00)
    private let negativeColor = UIColor(red: 0.00, green: 0.74, blue: 0.60, alpha: 1.00)
    private var deaPoints = [CGFloat]()
    private var difPoints = [CGFloat]()
    private var macdPoints = [CGFloat]()
    private let kData: [KData]
    private let barWidth: CGFloat
    private let spacing: CGFloat

    init(kData: [KData], barWidth: CGFloat, spacing: CGFloat) {
        self.kData = kData
        self.barWidth = barWidth
        self.spacing = spacing
        super.init()

        var ema12: CGFloat = 0
        var ema26: CGFloat = 0
        var oldEma12: CGFloat = 0.0
        var oldEma26: CGFloat = 0.0
        var diff: CGFloat = 0
        var dea: CGFloat = 0
        var oldDea: CGFloat = 0.0
        var macd: CGFloat = 0

        (0..<kData.count).forEach {
            let closePrice = kData[$0].closePrice
            if $0 == 0 {
                ema12 = closePrice
                ema26 = closePrice
            } else {
                ema12 = (2 * closePrice + 11 * oldEma12) / 13
                ema26 = (2 * closePrice + 25 * oldEma26) / 27
            }

            diff = ema12 - ema26
            dea = (diff * 2 + oldDea * 8) / 10.0
            macd = (diff - dea) * 2
            oldEma12 = ema12
            oldEma26 = ema26
            oldDea = dea
            deaPoints.append(dea)
            difPoints.append(diff)
            macdPoints.append(macd)
        }
    }

    override func draw(canvas: Canvas, in rect: CGRect, offsetY: CGFloat, yRatio: CGFloat, animatingScale : CGFloat = 1) {
        guard let (start, end) = indexRangeIn(rect: rect) else {
            return
        }

        let deaPath = UIBezierPath()
        let difPath = UIBezierPath()
        var isFirst = true

        canvas.save()
        canvas.clip(to: rect)
        (start...end).forEach { index in
            let x = CGFloat(index) * (barWidth + spacing) * scale +  contentInset.left
            let deaY = convertY(deaPoints[index], offsetY: offsetY, yRatio: yRatio)
            let difY = convertY(difPoints[index], offsetY: offsetY, yRatio: yRatio)
            let currentDeaPoint = CGPoint(x: x, y: deaY)
            let currentDifPoint = CGPoint(x: x, y: difY)
            if isFirst {
                isFirst = false
                deaPath.move(to: currentDeaPoint)
                difPath.move(to: currentDifPoint)
            } else {
                deaPath.addLine(to: currentDeaPoint)
                difPath.addLine(to: currentDifPoint)
            }
            let startY = convertY(0, offsetY: offsetY, yRatio: yRatio)
            let endY = convertY(macdPoints[index], offsetY: offsetY, yRatio: yRatio)
            let middle = (startY + endY) / 2
            LineDrawable(startPoint: CGPoint(x: x, y: startY),
                    endPoint: CGPoint(x: x, y: endY * animatingScale + (1 - animatingScale) * middle),
                    lineWidth: barWidth * scale,
                    color: macdPoints[index] >= 0 ? positiveColor : negativeColor).draw(canvas: canvas)
        }
        PathDrawable(path: deaPath, lineWidth: 1, color: .red).draw(canvas: canvas)
        PathDrawable(path: difPath, lineWidth: 1, color: .blue).draw(canvas: canvas)
        canvas.restore()
    }

    private func midPoint(p1: CGPoint, p2: CGPoint) -> CGPoint {
        CGPoint(x: (p1.x + p2.x) / 2, y: (p1.y + p2.y) / 2)
    }

    private func controlPoint(p1: CGPoint, p2: CGPoint) -> CGPoint {
        var control = midPoint(p1: p1, p2: p2)
        let diffY = abs(p2.y - control.y)

        if(p1.y < p2.y) {
            control.y += diffY
        } else if (p1.y > p2.y) {
            control.y -= diffY
        }

        return control
    }

    private func convertY(_ y: CGFloat, offsetY: CGFloat, yRatio: CGFloat) -> CGFloat {
        let value = (y - offsetY) * yRatio + contentInset.bottom
        return value
    }

    override func yRangeIn(rect: CGRect) -> (min: CGFloat, max: CGFloat)? {
        guard let (start, end) = indexRangeIn(rect: rect) else {
            return nil
        }
        
        guard start < kData.count, end < kData.count, start >= 0, end >= 0, start <= end else {
            return nil
        }

        var minValue = CGFloat(LONG_LONG_MAX)
        var maxValue = -CGFloat(LONG_LONG_MAX)
        for i in start...end {
            if minValue > deaPoints[i] {
                minValue = deaPoints[i]
            }

            if minValue > difPoints[i] {
                minValue = difPoints[i]
            }

            if minValue > macdPoints[i] {
                minValue = macdPoints[i]
            }

            if maxValue < deaPoints[i] {
                maxValue = deaPoints[i]
            }

            if maxValue < difPoints[i] {
                maxValue = difPoints[i]
            }

            if maxValue < macdPoints[i] {
                maxValue = macdPoints[i]
            }
        }

        return (min(0, minValue), maxValue)
    }

    private func indexRangeIn(rect: CGRect) -> (start: Int, end: Int)? {
        let unit = (barWidth + spacing) * scale
        let start = max(0, Int((rect.minX - contentInset.left / 2) / unit))
        let end = min(Int((rect.maxX - contentInset.left / 2) / unit) + 2, kData.count - 1)
        guard end < kData.count, start <= end, end >= 0, start >= 0 else {
            return nil
        }

        return (start, end)
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        guard index >= 0, index < difPoints.count else {
            return nil
        }
        let difString = NSMutableAttributedString(string: "DIF:\(String(format: "%.2lf", difPoints[index])) ", attributes: [.foregroundColor: difColor])
        let deaString = NSMutableAttributedString(string: "DEA:\(String(format: "%.2lf", deaPoints[index])) ", attributes: [.foregroundColor: deaColor])
        let macdString = NSMutableAttributedString(string: "MACD:\(String(format: "%.2lf", macdPoints[index]))", attributes: [.foregroundColor: UIColor.purple])
        difString.append(deaString)
        difString.append(macdString)

        return difString
    }
}
