//
// Created by Tony Wu on 2023/10/16.
//

import Foundation
import UIKit

class CCIDrawableConverter: PeriodDrawableConverter {
    override func getPoint(kData: [KData], period: Int) -> [CGFloat] {
        guard kData.count > 0 else { return [] }
        
        var tpList = [CGFloat]()
        var closeMaList = [CGFloat]()
        var cciValues = [CGFloat]()
        
        for i in 0...kData.count - 1  {
            var md: CGFloat = 0.0
            let n = 14
            
            let tp: CGFloat = (kData[i].highPrice + kData[i].lowPrice + kData[i].closePrice) / 3.0
            tpList.append(tp)
            closeMaList.append(kData[i].closePrice)
            let ma = tpList.suffix(n).reduce(0, +) / CGFloat(n)
            if i < n - 1 {
                for j in 0...i {
                    md += abs(ma - kData[j].closePrice)
                }
            } else {
                for j in (i - n + 1)...i {
                    md += abs(ma - tpList[j])
                }
            }
            md = md / CGFloat(n)
            
            let cci = (md != 0.0) ? (tp - ma) / md / 0.015 : 0.0
            cciValues.append(cci)
        }
        return cciValues
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        guard let string = super.getInfo(index: index) else {
            return nil
        }

        let result = NSMutableAttributedString(string: "CCI:", attributes: string.attributes(at: 0, effectiveRange: nil))
        result.append(string)

        return result
    }
}
