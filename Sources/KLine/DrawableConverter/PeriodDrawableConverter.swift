//
// Created by Konami on 2023/10/25.
//

import UIKit

class PeriodDrawableConverter: DrawableConverter {
    override var contentWidth: CGFloat {
        interval * scale * CGFloat(points.count)
    }

    private var points = [CGFloat]()
    private let barWidth: CGFloat = 2
    private let spacing: CGFloat = 0.5
    private var interval: CGFloat {
        barWidth + spacing
    }

    private let kData: [KData]
    private let period: Int
    private let color: UIColor

    init(kData: [KData], period: Int, barWidth: CGFloat, spacing: CGFloat, color: UIColor) {
        self.kData = kData
        self.period = period
        self.color = color
        super.init()
        points = getPoint(kData: kData, period: period)
    }

    override func draw(canvas: Canvas, in rect: CGRect, offsetY: CGFloat, yRatio: CGFloat, animatingScale : CGFloat = 1) {
        guard let (start, end) = indexRangeIn(rect: rect) else {
            return
        }

        var isFirst = true
        let path = UIBezierPath()

        (start...end).forEach {
            let x = CGFloat($0 + period - 1) * interval * scale + contentInset.left
            let y = (points[$0] - offsetY) * yRatio + contentInset.bottom
            let currentPoint = CGPoint(x: x, y: y)
            if isFirst {
                isFirst = false
                path.move(to: currentPoint)
            } else {
                path.addLine(to: currentPoint)
            }
        }
        canvas.save()
        canvas.clip(to: rect)
        PathDrawable(path: path, lineWidth: 1, color: color).draw(canvas: canvas)
        canvas.restore()
    }

    private func midPoint(p1: CGPoint, p2: CGPoint) -> CGPoint {
        CGPoint(x: (p1.x + p2.x) / 2, y: (p1.y + p2.y) / 2)
    }

    private func controlPoint(p1: CGPoint, p2: CGPoint) -> CGPoint {
        var control = midPoint(p1: p1, p2: p2)
        let diffY = abs(p2.y - control.y)

        if(p1.y < p2.y) {
            control.y += diffY
        } else if (p1.y > p2.y) {
            control.y -= diffY
        }

        return control
    }

    override func yRangeIn(rect: CGRect) -> (min: CGFloat, max: CGFloat)? {
        guard let (start, end) = indexRangeIn(rect: rect) else {
            return nil
        }

        var min = CGFloat(LONG_LONG_MAX)
        var max = -CGFloat(LONG_LONG_MAX)
        for i in start...end {
            if min > points[i] {
                min = points[i]
            }

            if max < points[i] {
                max = points[i]
            }
        }

        return (min, max)
    }

    private func indexRangeIn(rect: CGRect) -> (start: Int, end: Int)? {
        let unit = interval * scale
        let start = max(Int((rect.minX - contentInset.left / 2) / unit) - period , 0)
        let end = min(Int((rect.maxX - contentInset.left / 2) / unit) - period + 2, points.count - 1)
        guard end < points.count, start <= end, end >= 0, start >= 0 else {
            return nil
        }
        return (start, end)
    }

    func getPoint(kData: [KData], period: Int) -> [CGFloat] {
        []
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        let i = index - period + 1
        guard i >= 0, i < points.count else {
            return nil
        }
        let string = String(format: "%.2lf ", points[i])
        return NSMutableAttributedString(string: string, attributes: [.foregroundColor: color])
    }
}
