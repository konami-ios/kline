//
// Created by Tony Wu on 2023/10/16.
//

import Foundation
import UIKit

class WRDrawableConverter: PeriodDrawableConverter {
    override func getPoint(kData: [KData], period: Int) -> [CGFloat] {
        
        let n = 14
        var h = 0.0
        var l = 0.0
        
        return kData.enumerated().compactMap { (i, data) in
            let closePrice = data.closePrice
            let highPrice = data.highPrice
            let lowPrice = data.lowPrice
            
            if i < n {
                h = max(highPrice, h)
                l = min(lowPrice, l)
            } else {
                let prevNData = kData.prefix(i+1).suffix(n)
                h = prevNData.map { $0.highPrice }.max() ?? 0
                l = prevNData.map { $0.lowPrice }.min() ?? 0
            }
            
            if h - l == 0 {
                return 0
            } else {
                return (h - closePrice) / (h - l) * -100
            }
        }
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        guard let string = super.getInfo(index: index) else {
            return nil
        }

        let result = NSMutableAttributedString(string: "WR:", attributes: string.attributes(at: 0, effectiveRange: nil))
        result.append(string)

        return result
    }
}
