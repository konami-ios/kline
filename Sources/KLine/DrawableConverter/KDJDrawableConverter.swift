//
// Created by Tony Wu on 2023/10/16.
//

import Foundation
import UIKit

class KDJDrawableConverter: PeriodDrawableConverter {
    enum KDJType {
        case k
        case d
        case j
    }

    private var kResult = [CGFloat]()
    private var dResult = [CGFloat]()
    private var jResult = [CGFloat]()
    private let type: KDJType
    init(kData: [KData], period: Int, barWidth: CGFloat, spacing: CGFloat, color: UIColor, type: KDJType) {
        self.type = type
        super.init(kData: kData, period: period, barWidth: barWidth, spacing: spacing, color: color)
    }

    override func getPoint(kData: [KData], period: Int) -> [CGFloat] {
        calculateKDJ(data: kData, period: period)
        switch type {
        case .k:
            return kResult
        case .d:
            return dResult
        case .j:
            return jResult
        }
    }

    func calculateKDJ(data: [KData], period: Int) {
        guard period - 1 < data.count else {
            return 
        }

        var previousK: CGFloat? = nil
        var previousD: CGFloat? = nil
        
        for i in period - 1 ..< data.count {
            let cn = data[i].closePrice
        
            let pastData = Array(data[i - (period - 1) ... i])
            let ln = pastData.min { $0.lowPrice < $1.lowPrice }!.lowPrice
            let hn = pastData.max { $0.highPrice < $1.highPrice }!.highPrice
            let bottom: CGFloat = hn == ln ? 1 : (hn - ln )
            let rsv = (cn - ln) / bottom * 100
            
            let k = 1.0 / 3.0 * rsv + 2.0 / 3.0 * (previousK ?? 50)
            let d = 1.0 / 3.0 * k + 2.0 / 3.0 * (previousD ?? 50)
            previousK = k
            previousD = d
            let j = 3.0 * k - 2.0 * d
            kResult.append(k)
            dResult.append(d)
            jResult.append(j)
        }
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        guard let string = super.getInfo(index: index) else {
            return nil
        }

        let title: String
        switch type {
        case .k:
            title = "K:"
        case .d:
            title = "D:"
        case .j:
            title = "J:"
        }

        let result = NSMutableAttributedString(string: title, attributes: string.attributes(at: 0, effectiveRange: nil))
        result.append(string)

        return result
    }
}
