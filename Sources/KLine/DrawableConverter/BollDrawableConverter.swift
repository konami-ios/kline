//
//  BollDrawableConverter.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/6.
//

import Foundation
import UIKit

class BollDrawableConverter: PeriodDrawableConverter {
    enum BollType {
        case up
        case ma
        case dn
    }

    private var upPoints = [CGFloat]()
    private var maPoints = [CGFloat]()
    private var dnPoints = [CGFloat]()
    private let type: BollType
    init(kData: [KData], period: Int, barWidth: CGFloat, spacing: CGFloat, color: UIColor, type: BollType) {
        self.type = type
        super.init(kData: kData, period: period, barWidth: barWidth, spacing: spacing, color: color)
    }

    override func getPoint(kData: [KData], period: Int) -> [CGFloat] {
        calPoint(kData: kData, period: period)
        switch type {
        case .up:
            return upPoints
        case .ma:
            return maPoints
        case .dn:
            return dnPoints
        }
    }

    private func calPoint(kData: [KData], period: Int) {
        guard kData.count >= period else {
            return
        }

        var up: CGFloat = 0
        var dn: CGFloat = 0

        (period - 1..<kData.count).forEach {
            let ma = (kData[$0 - period + 1...$0].reduce(CGFloat(0)) { $0 + $1.closePrice } / CGFloat(period))
            let md = getBollMd(data: Array(kData[($0 - period + 1) ... ($0)]), ma: ma)
            up = ma + 2 * md
            dn = ma - 2 * md

            upPoints.append(up)
            maPoints.append(ma)
            dnPoints.append(dn)
        }
    }

    private func getBollMd(data: [KData], ma: CGFloat) -> CGFloat {
        var sum = 0.0
        (0 ..< data.count).forEach {
            let closeMa = data[$0].closePrice - ma
            sum += closeMa * closeMa
        }

        let b = sum > 0
        sum = abs(sum)
        let md = sqrt(sum / CGFloat(data.count))

        return b ? md : -1 * md
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        guard let string = super.getInfo(index: index) else {
            return nil
        }

        let title: String
        switch type {
        case .up:
            title = "UPPER:"

        case .ma:
            title = "MID:"
        case .dn:
            title = "LOWER:"
        }

        let result = NSMutableAttributedString(string: title, attributes: string.attributes(at: 0, effectiveRange: nil))
        result.append(string)

        return result
    }
}
