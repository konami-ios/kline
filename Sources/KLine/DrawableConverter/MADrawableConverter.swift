//
//  AverageDataConverter.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/3.
//

import Foundation
import UIKit

class MADrawableConverter: PeriodDrawableConverter {
    private let period: Int
    private let color: UIColor

    override init(kData: [KData], period: Int, barWidth: CGFloat, spacing: CGFloat, color: UIColor) {
        self.color = color
        self.period = period
        super.init(kData: kData, period: period, barWidth: barWidth, spacing: spacing, color: color)
    }

    override func getPoint(kData: [KData], period: Int) -> [CGFloat] {
        guard kData.count >= period else {
            return []
        }

        var maValues: [CGFloat] = []
        for i in period - 1..<kData.count {
            var sum: CGFloat = 0.0
            for j in (i - period + 1)...i {
                sum += kData[j].closePrice
            }

            // Calculate the moving average
            let ma = sum / CGFloat(period)
            maValues.append(ma)
        }

        return maValues
    }

    override func getInfo(index: Int) -> NSMutableAttributedString? {
        guard let superString = super.getInfo(index: index) else {
            return nil
        }
        let result = NSMutableAttributedString(string: "MA\(period):", attributes: [.foregroundColor: color])
        result.append(superString)

        return result
    }
}
