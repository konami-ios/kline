//
//  DataConverter.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/3.
//

import UIKit
class DrawableConverter {
    var scale: CGFloat = 1
    var contentWidth: CGFloat {
        0
    }
    var contentInset: UIEdgeInsets = .zero

    func yRangeIn(rect: CGRect) -> (min: CGFloat, max: CGFloat)? {
        nil
    }

    func getInfo(index: Int) -> NSMutableAttributedString? {
        nil
    }

    func draw(canvas: Canvas, in rect: CGRect, offsetY: CGFloat, yRatio: CGFloat, animatingScale : CGFloat = 1) {

    }
}
