//
//  LineDrawable.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/11.
//

import Foundation
import UIKit

class LineDrawable: Drawable {
    private let startPoint: CGPoint
    private let endPoint: CGPoint
    private let lineWidth: CGFloat
    private let color: UIColor

    init(startPoint: CGPoint, endPoint: CGPoint, lineWidth: CGFloat, color: UIColor) {
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.lineWidth = lineWidth
        self.color = color
    }

    func draw(canvas: Canvas) {
        canvas.drawLine(startPoint, endPoint, lineWidth: lineWidth, color: color)
    }
}