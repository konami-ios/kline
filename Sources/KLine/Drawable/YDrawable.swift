//
//  NewYDrawable.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/4.
//

import Foundation
import UIKit

class YDrawable: Drawable {
    private let min: CGFloat
    private let max: CGFloat
    private let targetRect: CGRect
    private let contentInset: UIEdgeInsets
    private let focusY: CGFloat?
    private let unit: CGFloat
    private let heightPerUnit: CGFloat

    private let textColor = UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1.00)
    private let textSize: CGFloat = 8
    private let focusLineColor = UIColor(red: 0.33, green: 0.33, blue: 0.33, alpha: 1.00)
    private let focusBackgroundColor = UIColor(red: 0.33, green: 0.33, blue: 0.33, alpha: 1.00)
    private let currentPriceBackgroundColor = UIColor(red: 1.00, green: 0.42, blue: 0.38, alpha: 1.00)
    private let entryPriceColor = UIColor(red: 0.85, green: 0.00, blue: 0.11, alpha: 1.00)
    private let sellPriceColor = UIColor(red: 0.00, green: 0.00, blue: 1.00, alpha: 1.00)
    private let dividerColor = UIColor(red: 0.96, green: 0.97, blue: 0.97, alpha: 1.00)
    private let offset: CGFloat = 2
    
    var bottomInset: CGFloat = 0
    var currentPrice: CGFloat?
    var entryPrices: [CGFloat]?
    var sellPrices: [CGFloat]?
    var showEntry = false
    
    init(min: CGFloat,max: CGFloat, targetRect: CGRect, contentInset: UIEdgeInsets, focusY: CGFloat? = nil) {
        self.min = min
        self.max = max
        self.targetRect = targetRect
        self.contentInset = contentInset
        self.focusY = focusY
        if max == min {
            unit = max / 5
        } else {
            unit = (max - min) / 5
        }
        heightPerUnit = (targetRect.height - contentInset.bottom - contentInset.top) / 5
    }

    func draw(canvas: Canvas) {
        guard unit > 0 else {
            return
        }
        var y = CGFloat(ceil(min / unit)) * unit
        
        while convertY(y) < targetRect.maxY {
            drawText(canvas: canvas, y: y, textColor: textColor)
            y += unit
        }
        canvas.drawLine(CGPoint(x: targetRect.maxX, y: targetRect.minY + contentInset.bottom), CGPoint(x: targetRect.maxX, y: targetRect.maxY), lineWidth: 1, color: textColor)

        if let currentPrice = currentPrice,
           currentPrice >= min,
           currentPrice <= max {
            drawHighlightText(canvas: canvas, y: currentPrice, textColor: .white, backgroundColor: currentPriceBackgroundColor)
        }

        if showEntry {
            entryPrices?.forEach {
                drawDashHighlightText(canvas: canvas, y: $0, textColor: .white, backgroundColor: entryPriceColor)
            }

            sellPrices?.forEach {
                drawDashHighlightText(canvas: canvas, y: $0, textColor: .white, backgroundColor: sellPriceColor)
            }
        }
        
        if let focusY = focusY {
            drawHighlightText(canvas: canvas, y: focusY, textColor: .white, backgroundColor: focusBackgroundColor)
        }
    }

    private func convertY(_ y: CGFloat) -> CGFloat {
        (y - min) / unit * heightPerUnit + contentInset.bottom
    }
    
    private func drawText(canvas: Canvas, y: CGFloat, textColor: UIColor) {
        let yHeight = convertY(y)
        let yPath = UIBezierPath()
        yPath.move(to: CGPoint(x: targetRect.minX, y: yHeight))
        yPath.addLine(to: CGPoint(x: targetRect.minX + targetRect.width, y: yHeight))
        canvas.drawPath(path: yPath, lineWidth: 1, color: dividerColor)
        
        let string = String(format: "%.2lf", y)
        let attributedString = NSAttributedString(string: string, attributes: [.font: UIFont.systemFont(ofSize: textSize, weight: .bold), .foregroundColor: textColor])
        let textRect = attributedString.boundingRect(with: CGSize(width: 100, height: 100), context: nil).insetBy(dx: -1, dy: 0)
        canvas.drawText(string, center: CGPoint(x: targetRect.maxX + textRect.width / 2 + offset, y: yHeight), textSize: textSize, weight: .bold, color: textColor)
    }
    
    private func drawHighlightText(canvas: Canvas, y: CGFloat, textColor: UIColor, backgroundColor: UIColor) {
        let yHeight = convertY(y)
        let yPath = UIBezierPath()
        yPath.move(to: CGPoint(x: targetRect.minX, y: yHeight))
        yPath.addLine(to: CGPoint(x: targetRect.minX + targetRect.width, y: yHeight))
        canvas.drawPath(path: yPath, lineWidth: 1, color: backgroundColor)
        
        let string = String(format: "%.2lf", y)
        let attributedString = NSAttributedString(string: string, attributes: [.font: UIFont.systemFont(ofSize: textSize, weight: .bold), .foregroundColor: textColor])
        let textRect = attributedString.boundingRect(with: CGSize(width: 100, height: 100), context: nil).insetBy(dx: -1, dy: 0)
        canvas.drawRect(CGRect(x: targetRect.maxX + offset, y: yHeight - textRect.height / 2, width: textRect.width, height: textRect.height), color: backgroundColor)
        canvas.drawText(string, center: CGPoint(x: targetRect.maxX + textRect.width / 2 + offset, y: yHeight), textSize: textSize, weight: .bold, color: .white)
    }
    
    private func drawDashHighlightText(canvas: Canvas, y: CGFloat, textColor: UIColor, backgroundColor: UIColor) {
        let yHeight = convertY(y)
        let yPath = UIBezierPath()
//        yPath.move(to: CGPoint(x: targetRect.minX, y: yHeight))
//        yPath.addLine(to: CGPoint(x: targetRect.minX + targetRect.width, y: yHeight))
        canvas.drawDashLine(CGPoint(x: targetRect.minX, y: yHeight), CGPoint(x: targetRect.minX + targetRect.width, y: yHeight), lineWidth: 0.5, color: backgroundColor)
//        canvas.drawPath(path: yPath, lineWidth: 1, color: backgroundColor)
        
        let string = String(format: "%.2lf", y)
        let attributedString = NSAttributedString(string: string, attributes: [.font: UIFont.systemFont(ofSize: textSize, weight: .bold), .foregroundColor: textColor])
        let textRect = attributedString.boundingRect(with: CGSize(width: 100, height: 100), context: nil).insetBy(dx: -1, dy: 0)
        canvas.drawRect(CGRect(x: targetRect.maxX + offset, y: yHeight - textRect.height / 2, width: textRect.width, height: textRect.height), color: backgroundColor)
        canvas.drawText(string, center: CGPoint(x: targetRect.maxX + textRect.width / 2 + offset, y: yHeight), textSize: textSize, weight: .bold, color: .white)
    }
}
