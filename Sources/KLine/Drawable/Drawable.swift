//
//  Drawable.swift
//  KLine
//
//  Created by Tony Wu on 2023/9/23.
//

import Foundation
protocol Drawable: AnyObject {
    func draw(canvas: Canvas)
}
