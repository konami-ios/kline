//
//  NewXDrawable.swift
//  KLine
//
//  Created by Tony Wu on 2023/10/4.
//

import Foundation
import UIKit

class XDrawable: Drawable {
    private let lineHeight: CGFloat = 4
    private let textSize: CGFloat = 8
    private let textColor = UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1.00)
    private let timestamps: [CGFloat]
    private let spacing: CGFloat
    private let contentInset: UIEdgeInsets
    private let dateFormatter = DateFormatter()
    private let focusLineColor = UIColor(red: 0.33, green: 0.33, blue: 0.33, alpha: 1.00)
    private let dividerColor = UIColor(red: 0.96, green: 0.97, blue: 0.97, alpha: 1.00)
    var focusIndex: Int?
    var drawTime = true


    init(timestamps: [CGFloat], spacing: CGFloat, contentInset: UIEdgeInsets,  point: CGPoint? = nil) {
        self.timestamps = timestamps
        self.spacing = spacing
        self.contentInset = contentInset
        dateFormatter.dateFormat = "MM-dd HH:mm"
    }

    func draw(canvas: Canvas) {
        canvas.save()
        canvas.clip(to: CGRect(x: canvas.contentOffset.x, y: 0, width: canvas.size.width - contentInset.right, height: canvas.size.height))
        let unit = spacing * canvas.scale
        let count: Int = 5 * 8 / Int(pow(2, floor(log2(canvas.scale))))
        let timeWidth = unit * CGFloat(count)
        let min = canvas.contentOffset.x
        let max = min + canvas.size.width

        var index = Int((min - contentInset.left) / timeWidth)
        if index < 0 {
            index = 0
        }
        let remaining = timeWidth - (min - timeWidth *  CGFloat(index))
        var left = min + remaining + contentInset.left - timeWidth

        while left < max && index * count < timestamps.count {
            let timestamp = timestamps[index * count]
            let string = dateFormatter.string(from: Date(timeIntervalSince1970: timestamp))
            canvas.drawLine(CGPoint(x: left, y: contentInset.bottom), CGPoint(x: left, y: canvas.size.height), lineWidth: 1, color: dividerColor)
            canvas.drawText(string, center: CGPoint(x: left, y: lineHeight - textSize + contentInset.bottom - 2), textSize: textSize, weight: .bold, color: textColor)
            left += timeWidth
            index += 1
        }

        canvas.drawLine(CGPoint(x: min, y: contentInset.bottom), CGPoint(x: max, y: contentInset.bottom), lineWidth: 1, color: textColor)

        guard let focusIndex = focusIndex else {
            return
        }
        let x = CGFloat(focusIndex) * unit + contentInset.left
        let xPath = UIBezierPath()
        xPath.move(to: CGPoint(x: x, y: contentInset.bottom))
        xPath.addLine(to: CGPoint(x: x, y: canvas.size.height))
        canvas.drawPath(path: xPath, lineWidth: 1, color: focusLineColor)


        if drawTime {
            let timestamp = timestamps[focusIndex]
            let string = dateFormatter.string(from: Date(timeIntervalSince1970: timestamp))
            let attributedString = NSAttributedString(string: string, attributes: [.font: UIFont.systemFont(ofSize: textSize, weight: .bold), .foregroundColor: textColor])
            let textRect = attributedString.boundingRect(with: CGSize(width: 100, height: 100), context: nil).insetBy(dx: -1, dy: 0)
            canvas.drawRect(CGRect(x: x - textRect.width / 2, y: lineHeight - textRect.height / 2, width: textRect.width, height: textRect.height), color: textColor)
            canvas.drawText(string, center: CGPoint(x: x, y: -lineHeight + contentInset.bottom - 2), textSize: textSize, weight: .bold, color: .white)
        }
    }
}
