//
//  CandleDrawable.swift
//  KLine
//
//  Created by Tony Wu on 2023/9/23.
//

import Foundation
import UIKit

class CandleDrawable: Drawable {
    let openPrice: CGFloat
    let lowPrice: CGFloat
    let highPrice: CGFloat
    let closePrice: CGFloat
    let volume: CGFloat
    let x: CGFloat
    let lineWidth: CGFloat
    let color: UIColor

    var isHighlight = false
    init(openPrice: CGFloat, lowPrice: CGFloat, highPrice: CGFloat, closePrice: CGFloat, volume: CGFloat, x: CGFloat, lineWidth: CGFloat, color: UIColor) {
        self.openPrice = openPrice
        self.lowPrice = lowPrice
        self.highPrice = highPrice
        self.closePrice = closePrice
        self.volume = volume
        self.x = x
        self.lineWidth = lineWidth
        self.color = color
    }

    func draw(canvas: Canvas) {
        if highPrice == lowPrice {
//            canvas.drawLine(CGPoint(x: x, y: highPrice + 5), CGPoint(x: x, y: lowPrice - 5) , lineWidth: 2, color: color)
        } else {
            canvas.drawLine(CGPoint(x: x, y: highPrice), CGPoint(x: x, y: lowPrice), lineWidth: 2, color: color)
        }

        if openPrice == closePrice {
            canvas.drawLine(CGPoint(x: x, y: openPrice + 1), CGPoint(x: x, y: closePrice - 1), lineWidth: lineWidth, color: color)
        } else {
            canvas.drawLine(CGPoint(x: x, y: openPrice), CGPoint(x: x, y: closePrice), lineWidth: lineWidth, color: color)
        }
    }
}
