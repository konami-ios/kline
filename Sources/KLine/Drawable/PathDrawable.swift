//
//  PathDrawable.swift
//  KLine
//
//  Created by Tony Wu on 2023/9/28.
//

import Foundation
import UIKit

class PathDrawable: Drawable {
    let path: UIBezierPath
    let lineWidth: CGFloat
    let color: UIColor

    init(path: UIBezierPath, lineWidth: CGFloat, color: UIColor) {
        self.path = path
        self.lineWidth = lineWidth
        self.color = color
    }

    func draw(canvas: Canvas) {
        canvas.drawPath(path: path, lineWidth: lineWidth, color: color)
    }
}