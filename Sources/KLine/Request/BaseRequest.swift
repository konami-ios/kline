//
// Created by Tony Wu(吳少華) on 2022/5/3.
//

import Foundation
import UIKit

class BaseRequest<Response> {
    // retry只處理GET Method
    private var retryTime: Int {
        if let httpMethod = urlRequest.httpMethod, httpMethod == "GET" {
            return 3
        } else {
            return 0
        }
    }

    private var retryCounter: Int = 0
    private let config = URLSessionConfiguration.default

    var urlRequest: URLRequest
    var callback: ((Response) -> Void)?
    var errorHandler: ((Int, String) -> Void)?

    init(path: String) {
        urlRequest = URLRequest(url: URL(string: String(format: "%@/%@", APIService.domain, path))!)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpMethod = "GET"
        urlRequest.timeoutInterval = 10
    }

    func execute() {
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: urlRequest) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
//        let task = URLSession.shared.dataTask(with: urlRequest) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            self.debug(data: data, response: response, error: error)
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {
                return
            }
            if statusCode >= 200 && statusCode <= 299,
                      let data = data {
                if let result = self.decode(data: data) {
                    self.requestDidSuccess(response: result)
                    self.callback?(result)
                } else if self.checkEmptyResponse(data: data){
                    self.httpOK()
                } else if self.retryCounter < self.retryTime {
                    self.retryCounter += 1
                    self.execute()
                } else if let customError = self.decode200Error(data: data) {
                    self.errorHandler?(customError.code, customError.message)
                }  else {
                    self.errorHandler?(-1, "\(self) decode failed")
                }
            } else if (statusCode == 403 || statusCode == 400 || statusCode == 404), let data = data,
                      let customError = self.decodeError(data: data)  {
                self.errorHandler?(customError.code, customError.message)
            } else {
                if self.retryCounter < self.retryTime {
                    self.retryCounter += 1
                    self.execute()
                } else {
                    if let error = error {
                        self.errorHandler?(error._code, error.localizedDescription)
                    } else {
                        self.errorHandler?(-1, "unknown error")
                    }
                }
            }
        }
        task.resume()
    }

    func decode(data: Data) -> Response? {
        //for implementing
        nil
    }

    func requestDidSuccess(response: Response) {
        //for implementing
    }

    func debug(data: Data?, response: URLResponse?, error: Error?) {
        //for debugging
    }

    func httpOK() {
        //for implementing
    }

    private func refreshToken() {
        print("token expired")
    }

    private func checkEmptyResponse(data: Data) -> Bool {
        data.isEmpty
    }

    private func decode200Error(data: Data) -> (code: Int, message: String)? {
        print("decode200Error")
        guard let json = try? JSONSerialization.jsonObject(with: data) as? [String: Any] else { return nil }
        guard let code = json["statusCode"] as? Int else { return nil }
        guard let result = json["result"] as? [String: Any] else { return nil }

        var message: String
        if let messages = result["message"] as? [String] {
            message = messages.first ?? ""
            return (code, message)
        } else if let content = result["message"] as? String {
            message = content
            return (code, message)
        }

        return nil
    }

    private func decodeError(data: Data) -> (code: Int, message: String)? {
        print("decodeError")
        guard let json = try? JSONSerialization.jsonObject(with: data) as? [String: Any] else { return nil }
        guard let code = json["statusCode"] as? Int else { return nil }
        guard let result = json["result"] as? [String: Any] else { return nil }

        var message: String  = ""
        if let messages = result["message"] as? [String] {
            message = messages.first ?? ""
        } else if let content = result["message"] as? String {
            message = content
        }

        return (code, message)
    }
}
