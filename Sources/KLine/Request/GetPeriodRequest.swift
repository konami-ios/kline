//
// Created by Tony Wu on 2023/10/17.
//

import Foundation

class GetPeriodRequest: BaseRequest<GetPeriodRequest.GetPeriodResponse> {
    class GetPeriodResponse: Decodable {
        let isSuccess: Bool
        let status: Int
        let message: String
        let data: Data

        enum CodingKeys: String, CodingKey {
            case isSuccess = "IsSuccess"
            case status = "Status"
            case message = "Message"
            case data = "Data"
        }

        class Data: Decodable {
            let list: [String]
            enum CodingKeys: String, CodingKey {
                case list = "List"
            }
        }
    }

    init() {
        super.init(path: APIService.getPeriod)
    }

    override func decode(data: Foundation.Data) -> GetPeriodResponse? {
        do {
            let model = try JSONDecoder().decode(GetPeriodResponse.self, from: data)
            return model
        } catch {
            print(error)
            return nil
        }
    }
}
