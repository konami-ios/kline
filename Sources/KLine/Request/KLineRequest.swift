//
// Created by Tony Wu on 2023/10/17.
//

import Foundation

class KLineRequest: BaseRequest<KLineRequest.KLineResponse> {
    class KLineResponse: Decodable {
        let isSuccess: Bool
        let status: Int
        let message: String
        let data: Data

        enum CodingKeys: String, CodingKey {
            case isSuccess = "IsSuccess"
            case status = "Status"
            case message = "Message"
            case data = "Data"
        }

        class Data: Decodable {
            let symbol: String
            let period: String
            let kLines: [KLine]
            let dailySummary: DailySummary

            enum CodingKeys: String, CodingKey {
                case symbol = "Symbol"
                case period = "Period"
                case kLines = "List"
                case dailySummary = "DailySummary"
            }

            class KLine: Decodable {
                let timestamp: CGFloat
                let open: CGFloat
                let high: CGFloat
                let low: CGFloat
                let close: CGFloat

                enum CodingKeys: String, CodingKey {
                    case timestamp = "T"
                    case open = "O"
                    case high = "H"
                    case low = "L"
                    case close = "C"
                }
            }

            class DailySummary: Decodable {
                let bid: CGFloat
                let open: CGFloat
                let high: CGFloat
                let low: CGFloat
                let close: CGFloat
                let priceChange: CGFloat
                let priceChangePercent: CGFloat

                enum CodingKeys: String, CodingKey {
                    case bid = "Bid"
                    case open = "Open"
                    case high = "High"
                    case low = "Low"
                    case close = "Close"
                    case priceChange = "PriceChange"
                    case priceChangePercent = "PriceChangePercent"
                }
            }
        }
    }

    init(symbol: String, period: String, size: Int) {
        super.init(path: APIService.kLine + "?Symbol=\(symbol)&Period=\(period)&Size=\(size)")
        print("KLine request symbol = \(symbol)")
    }

    override func decode(data: Foundation.Data) -> KLineResponse? {
        print("KLine Request decode")
        do {
            let model = try JSONDecoder().decode(KLineResponse.self, from: data)
            return model
        } catch {
            print(error)
            return nil
        }
    }
}
