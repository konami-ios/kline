//
// Created by Tony Wu(吳少華) on 2022/8/15.
//

import Foundation

final class APIService: NSObject {
    enum StatusCode {
        static let success: Int = 100
        static let failed: Int = 1010
        static let authFailed: Int = 200002
        static let argsError: Int = 1030
        static let infoNotFound: Int = 1040
        static let addFestivalRepeat: Int = 3010
        static let IVRFailed: Int = 4010
        static let unexpectedError: Int = 9999
        static let unauthorized: Int = -1
    }

    static var domain: String = "http://w36f689j.xyz"
    static let newestQuotation = "App/MarketData/CurrentKline"
    static let kLine = "App/MarketData/HistoricalKline"
    static let getPeriod = "App/MarketData/KlinePeriodList"
    static var instance = APIService()

    override private init() {
        super.init()
    }
}
