//
// Created by Tony Wu on 2023/10/17.
//

import Foundation

class NewestQuotationRequest: BaseRequest<NewestQuotationRequest.NewestQuotationResponse> {
    class NewestQuotationResponse: Decodable {
        let isSuccess: Bool
        let status: Int
        let message: String
        let data: Data

        enum CodingKeys: String, CodingKey {
            case isSuccess = "IsSuccess"
            case status = "Status"
            case message = "Message"
            case data = "Data"
        }

        class Data: Decodable {
            let symbol: String
            let period: String
            let date: String
            let timestamp: TimeInterval
            let open: CGFloat
            let high: CGFloat
            let low: CGFloat
            let close: CGFloat
            let updateTimestamp: TimeInterval
            let interval: Int

            enum CodingKeys: String, CodingKey {
                case symbol = "Symbol"
                case period = "Period"
                case date = "Date"
                case timestamp = "Timestamp"
                case open = "Open"
                case high = "High"
                case low = "Low"
                case close = "Close"
                case updateTimestamp = "UpdateTimestamp"
                case interval = "Interval"
            }
        }
    }

    private let symbol: String
    private let period: String

    init(symbol: String, period: String) {
        self.symbol = symbol
        self.period = period
        super.init(path: APIService.newestQuotation + "?Symbol=\(symbol)&Period=\(period)")
    }

    override func decode(data: Foundation.Data) -> NewestQuotationResponse? {
        do {
            let model = try JSONDecoder().decode(NewestQuotationResponse.self, from: data)
            return model
        } catch {
            print(error)
            return nil
        }
    }
}
