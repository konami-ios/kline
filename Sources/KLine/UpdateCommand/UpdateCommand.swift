//
// Created by Konami on 2023/10/17.
//

import Foundation

class UpdateCommand: KLineUpdateCommand {
    let title: String
    let id: String
    let timeInterval: TimeInterval

    init(title: String, id: String, timeInterval: TimeInterval) {
        self.title = title
        self.id = id
        self.timeInterval = timeInterval
    }
}
