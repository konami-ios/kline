//
// Created by Konami on 2023/10/17.
//

import Foundation

protocol KLineUpdateCommand {
    var title: String { get }
    var id: String { get }
    var timeInterval: TimeInterval { get }
}
